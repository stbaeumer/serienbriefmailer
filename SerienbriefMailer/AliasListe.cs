﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SerienbriefMailer
{
    public class Aliases : List<Alias>
    {
        public string SearchAndReturnAlias(string pPlaceholder, string pRecipient)
        {
            // Die Klammern werden entfernt, so sie in der CSV-Datei in der Kopfzeile enthalten sind. 

            pPlaceholder = pPlaceholder.Replace("[[", "").Replace("]]", "");

            for (int i = 0; i < (this[0].Rows).Length; i++)
            {
                // Falls der Platzhalter in der Kopfzeile existiert ...

                if ((this[0].Rows)[i] == pPlaceholder)
                {
                    // ... wird sein Index ermittelt ...

                    foreach (var value in this)
                    {
                        if (pRecipient == value.HeadRow)
                        {
                            // ... und der entsprechnde attributwert wird zuerückgegeben.

                            return value.Rows[i];
                        }
                    }
                }
            }
            return "";
        }
        
        public string SearchAndReturnPassword(string pRecipient)
        {
            // Die Klammern werden entfernt, so sie in der CSV-Datei in der Kopfzeile enthalten sind. 

            //pPlaceholder = pPlaceholder.Replace("[[", "").Replace("]]", "");

            try
            {
                for (int i = 0; i < (this[0].Rows).Length; i++)
                {
                    // Falls eine Spalte namens Password in der Kopfzeile existiert ...

                    if ((this[0].Rows)[i] == "Password")
                    {
                        // ... wird sein Index ermittelt ...

                        foreach (var value in this)
                        {
                            if (pRecipient.ToLower() == value.HeadRow.ToLower())
                            {
                                // ... und der entsprechnde attributwert wird zuerückgegeben.

                                return value.Rows[i];
                            }
                        }
                    }
                }
                return "";
            }
            catch (Exception)
            {
                return "";
            }
        }

        public Aliases()
        {
        }

        // Search text of current PDF page for any matches in CSV-file.
        // For each match the index of row and column is saved.
        // Only the column with most matches will be used.

        internal Mails AddOrModifyMailsBySearchingForTags(Mails pMails, string pCurrentPageText, string pSubject, string pBody, int fileNumber, int pageNumber)
        {
            // If headrow is "Password", the passowrd is set.



            // For each row in aliaslist ...

            foreach (var item in this)
            {
                
                string password = this.SearchAndReturnPassword(item.Rows[0].ToString());



                // ... and for each cell in row ...

                int columnIndex = 0;

                foreach (var cell in item.Rows)
                {
                    // ... if cell index not [0] (because [0] is the address itself and no tag) ...

                    if (cell != item.Rows[0])
                    {
                        // ... if any tag matches ...

                        string cellNacked = cell.Replace("\r", "");

                        if (cellNacked.Length > 0)
                        {
                            if (Regex.IsMatch(pCurrentPageText, string.Format(@"\b{0}\b", Regex.Escape(cellNacked))))
                            {
                                // ... the regular expressions are substituted ...

                                int rowIndex = pMails.GetIndex(item.Rows[0].ToString(), columnIndex);

                                // Try to return Password.

                                
                                
                                
                                // ... a new mail is being created ...

                                if (rowIndex < 0)
                                {
                                    pMails.Add(new Mail(item.Rows[0].ToString(), pSubject, pBody, new MailAttachment(item.Rows[0].ToString(), fileNumber, pageNumber), columnIndex, password));
                                    pMails.CountRecipientsSeletedByTag++;
                                }
                                else
                                {
                                    // ...or the attachment is being added to existing mail.

                                    pMails[rowIndex].mailAttachments.Add(new MailAttachment(item.Rows[0].ToString(), fileNumber, pageNumber));
                                }
                            }       
                        }
                    }
                    columnIndex++;
                }
            }
            return pMails;
        }

        internal string TooltipString(Aliases pAliases)
        {
            string toolTipString = "";

            int count = 0;
            
            foreach (Alias alias in pAliases)
            {
                string cell = "";

                foreach (var rows in alias.Rows)
                {
                    cell = cell + " " + rows;
                }

                string number = "      ";

                if (count > 0)
                {
                    number = "Nr." + count.ToString("000");
                }

                toolTipString = toolTipString + number + cell + "\n";
                count++;
            }

            return toolTipString;
        }

        internal void AddAliasesFromCsv(String[] rows)
        {
            // The first line is defined as headrow.
            bool rowIsheadrow = true;
            int numberOfCellsInRow = 0;
            int rowNumber = 0;

            string completeAliasInOneString = "";

            // In every row, ...

            foreach (var row in rows)
            {
                // ... as it is not empty ...

                if (row.Length > 3)
                {
                    // ... cells are seperated by comma or semicolon.

                    String[] cells = row.Split(',', ';');

                    foreach (var cell in cells)
                    {
                        cell.Replace("\r", "");
                    }

                    bool rowContainsMailAddress = Regex.IsMatch(cells[0], "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");

                    // The headline must not contain an e-mail-address, ...

                    if (rowIsheadrow && rowContainsMailAddress || !rowIsheadrow && !rowContainsMailAddress)
                    {

                        if (rowIsheadrow && rowContainsMailAddress)
                        {
                            throw new Exception("Die Kopfzeile darf keine E-Mail-Adresse enthalten. Eine gültig Kopfzeile könnte so aussehen:\n\nMail;Name;Ort");
                        }
                        if (!rowIsheadrow && !rowContainsMailAddress)
                        {
                            throw new Exception("Prüfen Sie Zeile " + (rowNumber + 1) + ". Es kann keine E-Mail-Adresse in der ersten Spalte ausgelesen werden.");
                        }

                        Global.CsvValid = false;
                        return;
                    }
                    try
                    {
                        // alias list is being filled from CSV-file.

                        this.Add(new Alias(cells[0].Replace("\"", "").Replace("'", "").ToLower(), row.Replace("\"", "").Replace("'", "").Replace("\r", "").Replace("\n", "")));

                        completeAliasInOneString = completeAliasInOneString + row.Replace("\"", "").Replace("'", "") + "\n";

                        rowNumber++;
                    }
                    catch (Exception ex)
                    {
                        Global.CsvValid = false;
                        throw new Exception("InterruptCsvReading" + rowNumber + "\n" + ex);
                    }

                    // If the number of columns of each row is different from headrow, processing is being stopped.

                    if (rowIsheadrow)
                    {
                        numberOfCellsInRow = cells.Length;
                    }

                    if (numberOfCellsInRow != cells.Length)
                    {
                        Global.CsvValid = false;
                        throw new Exception("Fehler in der CSV-Datei. Prüfen Sie die Anzahl der Spalten in Zeile " + rowNumber + ".");
                    }

                    // All rows after first looping are non-headrows.

                    rowIsheadrow = false;
                }
            }
            
            Properties.Settings.Default.aliasFile = completeAliasInOneString;
        }
    }
}
