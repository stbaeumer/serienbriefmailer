﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SerienbriefMailer
{
    public class MailAttachment
    {
        
        public MailAttachment(string pRecipient, int pFileNumber, int pPage)
        {            
            Recipient = pRecipient;
            FileNumber = pFileNumber;
            PageNumber = pPage;
        }

        public MailAttachment(string pFile)
        {
            
            File = pFile;
        }

        public string Recipient { get; set; }
        public int FileNumber { get; set; }
        public int PageNumber { get; set; }
        public Mail Email { get; set; }

        public string File { get; set; }
    }
}
