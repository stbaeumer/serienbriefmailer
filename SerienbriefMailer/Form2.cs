﻿using MetroFramework.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SerienbriefMailer
{
    public partial class Form2 : MetroForm
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void RecipientsByMail_Click(object sender, EventArgs e)
        {
            Global.RecipientsByMail = true;
            Global.RecipientsByTag = false;
            this.Close();
        }

        private void RecipientsByTag_Click(object sender, EventArgs e)
        {
            Global.RecipientsByMail = false;
            Global.RecipientsByTag = true;
            this.Close();
        }
    }
}
