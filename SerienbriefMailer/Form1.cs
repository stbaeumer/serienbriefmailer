﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Text.RegularExpressions;
using System.Collections;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Reflection;
using MetroFramework.Forms;
using System.DirectoryServices.AccountManagement;
using Ionic.Zip;
using System.Security.Cryptography;
using System.Net;
using PgpSharp.GnuPG;
using PgpSharp;
using Microsoft.Exchange.WebServices.Data;

namespace SerienbriefMailer
{
    public partial class frm : MetroForm
    {
        public frm()
        {
            InitializeComponent();
        }

        Mails mails = new Mails();
        Mails filteredMails = new Mails();
        Aliases aliases;
        MailAttachments mailAttachments = new MailAttachments();
	    Logs logs = new Logs();        
        Assembly assembly;
        ResourceManager r;
        BackgroundWorker sendMailWorker;
        // Check SMTP
        BackgroundWorker smtpWorker;
        RadioButton radioButton;

        IPgpTool tool = new GnuPGTool();
        
        // Complete CSV-file in a string for saving purpose.

        string csvFile;

        // Check if textchange is caused directly by user-interaction or by form_load etc.

        bool tbxBodyEnter = false;

        // Messages can be browsed before sending. -1 represents mail-template.

        int browseIndex = -1;

        // Boolean for making distinction between user interaction text change and code driven text change event.

        bool tbxSubjectEnter = false;

        private void Form1_Load(object sender, EventArgs e)
        {
            aliases = new Aliases();

            logs.Add(new Log(""));
            logs.Add(new Log("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"));
            logs.Add(new Log("+                                                                                                         +"));
            logs.Add(new Log("+                           This programs comes to you free of charge.                                    +"));
            logs.Add(new Log("+                                                                                                         +"));
            logs.Add(new Log("+                         Published under Affero General Public License.                                  +"));
            logs.Add(new Log("+                          http://www.gnu.org/licenses/agpl-3.0.de.html                                   +"));
            logs.Add(new Log("+                                                                                                         +"));
            logs.Add(new Log("+                    Powered by mighty iTextsharp engine. Thank you, iTextsharp.                          +"));
            logs.Add(new Log("+                           http://sourceforge.net/projects/itextsharp/                                   +"));
            logs.Add(new Log("+                                                                                                         +"));
            logs.Add(new Log("+           Drop-dead gorgeous icons are borrowed from Oxygen Project. Thank you, Oxygen.                 +"));
            logs.Add(new Log("+                             https://techbase.kde.org/Projects/Oxygen                                    +"));
            logs.Add(new Log("+                                                                                                         +"));
            logs.Add(new Log("+                                                                                                         +"));
            logs.Add(new Log("+                                 Stefan Bäumer " + DateTime.Now.Year + "                                                      +"));
            logs.Add(new Log("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"));
            logs.Add(new Log(""));

            // Discard everything at Form_Load.

            Global.MailSettingsValid = false;
            Global.PdfSuccessfullyReadIn = false;
            Global.CsvValid = false;
            GnuPGConfig.GnuPGExePath = @"C:\Program Files (x86)\GnuPG\bin\gpg.exe";
            settingLanguage();

            // Try reading CSV-file from Properties, if it has been saved in former sessions.

            string aliasstring = Properties.Settings.Default.aliasFile;

            if (aliasstring != "")
            {
                try
                {
                    String[] rows = aliasstring.Split(new[] { '\n' });

                    aliases.AddAliasesFromCsv(rows);
                    Global.CsvValid = true;

                    // Time to render groupBoxes.

                    RenderCsvGroupBox();
                    RenderSendMail();
                    RenderPdfGroupBox();
                    RenderMailGroupBox();
                    groupBoxCsvMain.Height = 122;
                    groupBoxCsv.Visible = true;
                }
                catch (Exception)
                {
                    groupBoxCsvMain.Height = 89;
                    groupBoxCsv.Visible = false;
                }
            }
            else
            {
                groupBoxCsvMain.Height = 89;
                groupBoxCsv.Visible = false;
            }

            // Read saved values.

            Global.Subject = Properties.Settings.Default.Subject;
            Global.Body = Properties.Settings.Default.Body;

            tbxSmtpServer.Text = Properties.Settings.Default.SmtpServer;
            Global.SmtpServer = Properties.Settings.Default.SmtpServer;

            tbxSmtpUserMail.Text = Properties.Settings.Default.SmtpUser;
            Global.SmtpUser = Properties.Settings.Default.SmtpUser;

            tbxSmtpPort.Text = Properties.Settings.Default.SmtpPort;
            Global.SmtpPort = Properties.Settings.Default.SmtpPort;
                        
            tbxSmtpPassword.Text = Properties.Settings.Default.SmtpPassword;
            Global.SmtpPassword = Properties.Settings.Default.SmtpPassword;

            chkSsl.Checked = Properties.Settings.Default.Ssl;
            Global.Ssl = Properties.Settings.Default.Ssl;

            checkExchange.Checked = Properties.Settings.Default.exchange;
            metroTextBox1.Text = Properties.Settings.Default.exchangeUrl;
            // Backgroundworker for sending mails process.

            sendMailWorker = new BackgroundWorker();
            sendMailWorker.WorkerReportsProgress = true;
            sendMailWorker.WorkerSupportsCancellation = true;

            sendMailWorker.DoWork += new DoWorkEventHandler(worker_DoWork);
            sendMailWorker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            sendMailWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);

            // Backgroundworker for checking SMTP.

            smtpWorker = new BackgroundWorker();
            smtpWorker.WorkerReportsProgress = true;
            smtpWorker.WorkerSupportsCancellation = true;

            smtpWorker.DoWork += new DoWorkEventHandler(worker1_DoWork);
            smtpWorker.ProgressChanged += new ProgressChangedEventHandler(worker1_ProgressChanged);
            smtpWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker1_RunWorkerCompleted);

            // If program is being started the first time ever.

            if (Properties.Settings.Default.firstStartEver)
            {
                UserPrincipal userPrincipal = UserPrincipal.Current;
                String name = userPrincipal.DisplayName;
                Global.Subject = r.GetString("firstStartSubject");
                Global.Body = r.GetString("firstStartBody") + name;
                Properties.Settings.Default.firstStartEver = false;
            }
            //if (Properties.Settings.Default.passphrase == "" || Properties.Settings.Default.passphrase == null)
            
            if(true)
            {
                Random random = new Random();
                random.Next(11, 111111111);
                Properties.Settings.Default.passphrase = "Hallo";// random.ToString();
                Properties.Settings.Default.Save();
            }

            toolTip1.SetToolTip(imgEncryptAttachment, r.GetString("MailsAreEncrypted"));
            
            toolTip1.SetToolTip(imgDecryptAttachment, r.GetString("AttachmentsWillNotBeEncryptedFullstopClickIfYouWantMoreSecurity"));
            lblSmtp.Text = r.GetString("verifying");
            smtpWorker.RunWorkerAsync();
        }

        private void settingLanguage()
        {
            assembly = Assembly.Load("SerienbriefMailer");
            string selectedLanguage = "SerienbriefMailer.Lang.de-DE";

            if (Properties.Settings.Default.SelectedLanguage == "en-UK")
            {
                selectedLanguage = "SerienbriefMailer.Lang.en-UK";
            }

            r = new ResourceManager(selectedLanguage, assembly);

            lblTitle.Text = r.GetString("BulkLetterMailer");
            lblCopyleft.Text = "\u00a9 " + DateTime.Now.Year + " " + r.GetString("BulkLetterMailer");
            lblPdfTitle.Text = r.GetString("PdfFileAsDatasourceForMessages");
            btnCSV.Text = r.GetString("NoCsvFileHasBeenUploadedYet");
            lblCsvTitle.Text = r.GetString("CsvFileForPersonalizationAndDeductionFullstop");
            lblSmtpTitle.Text = r.GetString("SmtpServerSettings");
            lblSmtpUserMail.Text = r.GetString("mail");
            lblSmtpPassword.Text = r.GetString("Password");
            lblSmtp.Text = r.GetString("SettingsAreCorrect");

            toolTip1.SetToolTip(imgSmtpMailaddressSave, r.GetString("OptionallySaveMailAddressForFutureSessions"));
            toolTip1.SetToolTip(imgSmtpServerSave, r.GetString("OptionallySaveServerAddressForFutureSessions"));
            toolTip1.SetToolTip(imgSmtpPortSave, r.GetString("OptionallySavePortForFutureSessions"));
            toolTip1.SetToolTip(imgSmtpPasswordSave, r.GetString("OptionallySavePasswordForFutureSessions"));

            // Render everything with new language.

            RenderCsvGroupBox();
            RenderPdfGroupBox();
            RenderMailGroupBox();
            RenderSendMail();
        }


        private void btnPdfFileSelected_Click(object sender, EventArgs e)
        {
            // Discard everything at btn_Click.
            
            progressBar.Value = progressBar.Minimum;
            lblStatusPercent.Text = "";
            mails = new Mails();
            filteredMails = new Mails();
            Global.PdfSuccessfullyReadIn = false;
            Global.SelectedColumn = -1;
            browseIndex = -1;
            groupBoxPdf.Height = 89;
            groupBoxPdfMail.Visible = false;
            groupBoxMail.Visible = false;
            btnPdfFileSelect.Text = r.GetString("NoPdfFileSelectedYet");
            imgPdfOkNot.Visible = false;
            imgPdfOk.Visible = false;
            openFileDialog.Filter = r.GetString("pdfFiles") + " (.pdf)|*.pdf";
            openFileDialog.FilterIndex = 1;
            

            DialogResult result = openFileDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                try
                {
                    // Create all mails from addresses and tags.

                    mails = mails.AddMails(openFileDialog.FileNames, aliases);

                    // Only one column must be source for tags, ... 

                    int numberOfPossibleColumns = (mails.GroupBy(m => m.Column) .Select(grp => grp.First()) .ToList()).Count();

                    // ... so if only one column matches (and at least one mail exists) everything is fine.

                    if (numberOfPossibleColumns == 1)
                    {
                        filteredMails = mails;
                        Global.PdfSuccessfullyReadIn = true;
                        Global.SelectedColumn = mails[0].Column;
                    }

                    // If mails have been read / deduced from more than one column, user has to decide.

                    if (numberOfPossibleColumns > 1)
                    {
                        Global.PdfSuccessfullyReadIn = true;
                    }
                }
                catch (Exception ex)
                {
                    Global.PdfSuccessfullyReadIn = false;
                    lblPdfFile.Visible = true;
                    lblPdfFile.Text = ex.Message + "\n" + ex.ToString();
                    toolTip1.SetToolTip(lblPdfFile, ex.Message);
                    imgPdfOkNot.Visible = true;
                    groupBoxPdf.Height = 122;
                    groupBoxPdfMail.Visible = true;
                    return;
                }         
            }
            else
            {
                Global.PdfSuccessfullyReadIn = false;
            }

            // Reset browseIndex and show mail-template.
                        
            RenderPdfGroupBox();
            RenderMailGroupBox();
            RenderSendMail();
            RenderCsvGroupBox();
        }

        
        private void radioButon_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton r = (RadioButton)sender;
            
            int tag = (Int32)r.Tag;

            filteredMails = new Mails();

            foreach (var mail in mails)
            {
                if (mail.Column == tag)
                {
                    filteredMails.Add(mail);
                }
            }

            Global.SelectedColumn = tag;
            RenderMailGroupBox();
            RenderPdfGroupBox();
            RenderSendMail();

            foreach (Control gb in this.Controls)
            {
                if (gb is GroupBox && gb.Name == "groupBoxPdf")
                {
                    foreach (Control xx in gb.Controls)
                    {
                        if (xx is RadioButton)
                        {
                            xx.Visible = false;
                        }
                    }
                }
            }
        }

        private void RenderSendMail()
        {
            if ((from m in filteredMails where m.ReadyForTakeOff select m).Count() > 0 && Global.MailSettingsValid && Global.SelectedColumn > -1)
            {
                var mailReadyForTakeOff = from m in filteredMails 
                                            where m.ReadyForTakeOff 
                                            select mails.ToList();

                btnSendMails.Text = mailReadyForTakeOff.Count() + " " + r.GetString("MailsReadyForSending");
                
                groupBoxSendMail.Visible = true;
                
                string tooltip = "";
                int number = 1;
                try
                {
                    foreach (var mail in filteredMails)
                    {
                        if (mail.ReadyForTakeOff)
                        {
                            tooltip = tooltip + number.ToString("000") + " " + mail.Recipient.Substring(0, System.Math.Min(14, mail.Recipient.Length)) + "...\t" + mail.Subject.Substring(0, System.Math.Min(30, mail.Subject.Length)) + "...\t" + "(" + mail.mailAttachments.Count() + " " + r.GetString("attachments") + ")\n";    
                        }
                        else
                        {
                            tooltip = tooltip + "---" + " " + mail.Recipient.Substring(0, System.Math.Min(14, mail.Recipient.Length)) + "...\t" + "++++" + r.GetString("deleted") + "++++" + "...\t" + "(" + mail.mailAttachments.Count() + " " + r.GetString("attachments") + ")\n";    
                        }
                        
                        number++;   
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                
                toolTip1.SetToolTip(btnSendMails, tooltip);
            }
            else
            {
                groupBoxSendMail.Visible = false;
            }
            if (!Global.MailSettingsValid)
            {
                //lblSmtp.Text = r.GetString("smtpConfigurationNotOk");
                imgSmtpOk.Visible = false;
                imgSmtpNotOk.Visible = true;
            }
        }

        private void RenderPdfGroupBox()
        {
            if (Global.PdfSuccessfullyReadIn)
            {   
                // Render button and tooltip

                btnPdfFileSelect.Text = openFileDialog.FileNames.Count() + " " + r.GetString("filesWith") + " " + Global.NumberOfPages + " " + r.GetString("pagesRead");
                string tooltip = "";
             
                foreach (var item in openFileDialog.FileNames)
                {
                    tooltip = tooltip + Path.GetFileName(item.ToString()) + "\n";
                }

                toolTip1.SetToolTip(btnPdfFileSelect, tooltip);
                
                // If a column has been selected.

                if (Global.SelectedColumn > -1)
                {
                    lblPdfFile.Visible = true;
                    groupBoxPdf.Height = 122;
                    groupBoxPdfMail.Visible = true;
                    imgPdfOk.Visible = true;
                    toolTip1.SetToolTip(lblPdfFile, mails.TooltipString(mails, 0, aliases));

                    if (Global.SelectedColumn == 0)
                    {
                        lblPdfFile.Text = filteredMails.Count() + " " + r.GetString("FoundAddresses");
                        toolTip1.SetToolTip(lblPdfFile, filteredMails.TooltipString(filteredMails, 0, aliases));
                    }
                    if (Global.SelectedColumn > 0)
                    {
                        int a = filteredMails.Count();
                        int b = filteredMails[0].Column;

                        lblPdfFile.Text = a + " " + r.GetString("eMailAddressFromCsvFile") + aliases[0].Rows[b].ToString().Replace("\r","") + r.GetString("deducedFullstop");
                        toolTip1.SetToolTip(lblPdfFile, filteredMails.TooltipString(filteredMails, Global.SelectedColumn, aliases));
                    }
                }

                // If no column has been selected by now ...

                if (Global.SelectedColumn == -1)
                {
                    var differentColumns = from m in mails group m by new { TagRelated = m.Column } into mygroup select mygroup.FirstOrDefault();

                    int numberOfSecetableColumns = differentColumns.Count();

                    // ... and there is more than one option ...

                    if (numberOfSecetableColumns > 0)
                    {
                        // Delete all radiobuttons, if existing

                        foreach (Control gb in this.Controls)
                        {
                            if (gb is GroupBox && gb.Name == "groupBoxPdf")
                            {
                                foreach (Control xx in gb.Controls)
                                {
                                    if (xx is RadioButton)
                                    {
                                        xx.Visible = false;
                                    }
                                }
                            }
                        }
                        
                        
                        // Radiobuttons are generated and shown.

                        groupBoxPdfMail.Visible = true;
                        imgColumnDecision.Visible = true;
                        lblPdfFile.Visible = true;
                        lblPdfFile.Text = r.GetString("futherOptions") + " " + differentColumns.Count() + " " + r.GetString("makeDecision");
                        toolTip1.SetToolTip(lblPdfFile, r.GetString(""));

                        int i = 100;
                        groupBoxPdf.Height = 171;

                        foreach (var item in differentColumns)
                        {
                            var counter = (from m in mails where m.Column == item.Column select m).Count();

                            // The cell must not be empty

                            var index = aliases.FindIndex(e => e.Rows[0] == item.Recipient);

                            int lenght = 1;

                            try
                            {
                                lenght = aliases[index].Rows[item.Column].ToString().Replace("\r", "").Length;
                            }
                            catch (Exception)
                            {
                            }

                            if (lenght > 0)
                            {
                                radioButton = new RadioButton();

                                radioButton.CheckedChanged += new System.EventHandler(this.radioButon_CheckedChanged);

                                if (item.Column == 0)
                                {
                                    radioButton.Text = counter + " " + r.GetString("toThoseRecipientsThatCanBeFoundDirectly");
                                    toolTip1.SetToolTip(radioButton, mails.TooltipString(mails, 0, aliases));
                                }
                                else
                                {
                                    radioButton.Text = counter + " " + r.GetString("toThoseRecipientsThatCanBeDeducedFrom") + aliases[0].Rows[item.Column].ToString().Replace("\r", "") + r.GetString("deducedFrom");
                                    toolTip1.SetToolTip(radioButton, filteredMails.TooltipString(mails, item.Column, aliases));
                                }

                                i = i + 30;
                                radioButton.Location = new Point(13, i);
                                radioButton.Tag = item.Column;

                                radioButton.Width = groupBoxPdf.Width - 20;
                                radioButton.Checked = false;
                                groupBoxPdf.Controls.Add(radioButton);
                                groupBoxPdf.Height = radioButton.Location.Y + 50;                
                            }
                        }
                        imgPdfOk.Visible = false;
                        groupBoxPdfMail.Visible = true;
                    }


                    if (numberOfSecetableColumns == 1)
                    {
                        foreach (var item in differentColumns)
                        {
                            Global.SelectedColumn = item.Column;
                        }

                        lblPdfFile.Visible = true;
                        groupBoxPdf.Height = 122;
                        groupBoxPdfMail.Visible = true;
                        imgPdfOk.Visible = true;
                        toolTip1.SetToolTip(lblPdfFile, mails.TooltipString(mails, 0, aliases));

                        if (Global.SelectedColumn == 0)
                        {
                            lblPdfFile.Text = filteredMails.Count() + " " + r.GetString("FoundAddresses");
                            toolTip1.SetToolTip(lblPdfFile, filteredMails.TooltipString(filteredMails, 0, aliases));
                        }
                        if (Global.SelectedColumn > 0)
                        {
                            lblPdfFile.Text = filteredMails.Count() + " " + r.GetString("eMailAddressFromCsvFile") + aliases[0].Rows[filteredMails[0].Column].ToString().Replace("\r", "") + r.GetString("deducedFullstop");
                            toolTip1.SetToolTip(lblPdfFile, filteredMails.TooltipString(filteredMails, Global.SelectedColumn, aliases));
                        }
                    }

                    // If PDF-file is readable, but returns no address.

                    if (mails.Count == 0)
                    {
                        groupBoxPdfMail.Visible = true;
                        lblPdfFile.Visible = true;
                        groupBoxPdf.Height = 122;
                        lblPdfFile.Text = r.GetString("NoAddressFound");
                        toolTip1.SetToolTip(lblPdfFile, r.GetString("NoAddressFound"));
                        imgPdfOkNot.Visible = true;
                    }
                }
            }
            if (!Global.PdfSuccessfullyReadIn)
            {
                btnPdfFileSelect.Text = r.GetString("NoPdfFileSelectedYet");
                lblPdfFile.Visible = false;
                imgPdfOkNot.Visible = false;
                imgPdfOk.Visible = false;
                groupBoxPdf.Height = 89;
                groupBoxPdfMail.Visible = false;
                toolTip1.SetToolTip(btnPdfFileSelect, "");
            }
        }

        private void btnSendMails_Click(object sender, EventArgs e)
        {
            if (sendMailWorker.IsBusy)
            {
                sendMailWorker.CancelAsync();
                btnSendMails.Text = r.GetString("MailsReadyForSending");
            }
            else
            {
                if (progressBar.Value == progressBar.Maximum)
                {
                    progressBar.Value = progressBar.Minimum;
                }
                sendMailWorker.RunWorkerAsync(progressBar.Value);

                btnSendMails.Text = r.GetString("pause");
            }
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;

            if (e.ProgressPercentage < 100)
            {
                lblStatusPercent.Text = (e.ProgressPercentage + 1) + "%";
            }
            
            lblStatus.Text = ((SerienbriefMailer.Mail)(e.UserState)).Recipient + " " + ((SerienbriefMailer.Mail)(e.UserState)).Success;                    
            tbxSubject.Text = ((SerienbriefMailer.Mail)(e.UserState)).Subject;
            tbxBody.Text = ((SerienbriefMailer.Mail)(e.UserState)).Body;

            string attachmentDeclined = r.GetString("attachment");

            if (((SerienbriefMailer.Mail)(e.UserState)).mailAttachments.Count > 1)
            {
                attachmentDeclined = r.GetString("attachments");
            }

            lblEmail.Text = ((SerienbriefMailer.Mail)(e.UserState)).Recipient + " (" + ((SerienbriefMailer.Mail)(e.UserState)).mailAttachments.Count + " " + attachmentDeclined + ")";
            
            if (e.ProgressPercentage == 100)
            {
                RenderMailGroupBox();
            }
        }

        int z = 0;

        private static bool RedirectionUrlValidationCallback(string redirectionUrl)
        {
            // The default for the validation callback is to reject the URL.
            bool result = false;

            Uri redirectionUri = new Uri(redirectionUrl);

            // Validate the contents of the redirection URL. In this simple validation
            // callback, the redirection URL is considered valid if it is using HTTPS
            // to encrypt the authentication credentials. 
            if (redirectionUri.Scheme == "https")
            {
                result = true;
            }
            return result;
        }

        void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            int prozentBeendet = (int)e.Argument;

            var onlyReadyForTakOffMails = from m in filteredMails where m.ReadyForTakeOff == true select m;

            while (!sendMailWorker.CancellationPending && prozentBeendet <= System.Math.Max(onlyReadyForTakOffMails.Count(),100))
            {
                int p = prozentBeendet;
                
                double m = onlyReadyForTakOffMails.Count();
                
                double q = (100 / m) * z;


                if (p >= q && z < onlyReadyForTakOffMails.ToList().Count)
                //if (z < onlyReadyForTakOffMails.ToList().Count)
                {
                    MemoryStream memorystreamIn = new MemoryStream();
                    MemoryStream memorystreamOut = new MemoryStream();
                    
                    try
                    {
                        string body = onlyReadyForTakOffMails.ToList()[z].Body;
                        body = body.Replace("\r\n", "<br/>");
                        body = body + "<br/><br/><br/><br/><br/><br/><br/><hr>" + r.GetString("MailDispatchWith");

                        if (Properties.Settings.Default.exchange)
                        {
                             System.Net.ServicePointManager.ServerCertificateValidationCallback = ((ender, certificate, chain, sslPolicyErrors) => true);
                            ExchangeService service = new ExchangeService();
                            service.UseDefaultCredentials = true;
                            service.TraceEnabled = false;
                            service.TraceFlags = TraceFlags.All;
                            service.Url = new Uri(Properties.Settings.Default.exchangeUrl);
                            EmailMessage neueMail = new EmailMessage(service);

                            // Set properties on the email message.
                            neueMail.ToRecipients.Add(new EmailAddress(onlyReadyForTakOffMails.ToList()[z].Recipient));
                            neueMail.BccRecipients.Add(new EmailAddress(Global.SmtpUser));
                            neueMail.Subject = onlyReadyForTakOffMails.ToList()[z].Subject;
                            neueMail.Body = body;

                            foreach (var mailAttachment in onlyReadyForTakOffMails.ToList()[z].mailAttachments)
                            {
                                try
                                {
                                    string file = openFileDialog.FileNames[mailAttachment.FileNumber - 1].ToString();
                                    int pageNumber = mailAttachment.PageNumber;

                                    memorystreamIn = new MemoryStream(ExtractPages(file, pageNumber, pageNumber));

                                    // If encryption is wanted ...

                                    if (onlyReadyForTakOffMails.ToList()[z].EncryptAttachment)
                                    {
                                        // Wenn ein GPG-Key vorhanden ist, wird mit ihm verschlüsselt.
                                        // Nachrangig wird symmetrisch verschlüsselt.

                                        if (onlyReadyForTakOffMails.ToList()[z].PublicKey != null)
                                        {
                                            string datei = "U:\\" + Path.GetFileNameWithoutExtension(file) + "-" + r.GetString("page") + mailAttachment.PageNumber + ".pdf";

                                            FileStream fileStream = new FileStream(datei, FileMode.Create, FileAccess.Write);
                                            memorystreamIn.WriteTo(fileStream);
                                            fileStream.Close();

                                            var encryptArg = new FileDataInput();

                                            encryptArg.Armor = true;
                                            encryptArg.InputFile = datei;
                                            encryptArg.OutputFile = "U:\\" + Path.GetFileNameWithoutExtension(file) + "-" + r.GetString("page") + mailAttachment.PageNumber + ".pdf" + ".pgp";
                                            encryptArg.Operation = DataOperation.Encrypt;
                                            encryptArg.Recipient = onlyReadyForTakOffMails.ToList()[z].PublicKey.Id;
                                            try
                                            {
                                                tool.ProcessData(encryptArg);
                                                neueMail.Attachments.AddFileAttachment("U:\\" + Path.GetFileNameWithoutExtension(file) + "-" + r.GetString("page") + mailAttachment.PageNumber + ".pdf" + ".pgp");
                                            }
                                            catch (Exception ex)
                                            {
                                                throw new Exception(ex.ToString());
                                            }                                            
                                        }
                                        else
                                        {
                                            using (ZipFile zip = new ZipFile())
                                            {
                                                // http://shahvaibhav.com/create-zip-file-in-memory-using-dotnetzip/

                                                MemoryStream ms = new MemoryStream();

                                                // ... a password is been set to elstablish encryption.

                                                zip.Password = onlyReadyForTakOffMails.ToList()[z].Password;

                                                // 0 means no compression

                                                zip.CompressionLevel = 0;

                                                // Unencrypted memorystream is added to zip.

                                                zip.AddEntry(Path.GetFileName(file), memorystreamIn);
                                                //zip.AddEntry(file, memorystreamIn);

                                                // Alternative algorithm may not work with standard zip app.

                                                // zip.Encryption = EncryptionAlgorithm.WinZipAes256;

                                                zip.Save(ms);
                                                ms.Position = 0;
                                                System.IO.File.WriteAllBytes(Path.GetFileNameWithoutExtension(file) + "-" + r.GetString("page") + mailAttachment.PageNumber + ".pdf" + ".zip", ms.ToArray());

                                                neueMail.Attachments.AddFileAttachment(Path.GetFileNameWithoutExtension(file) + "-" + r.GetString("page") + mailAttachment.PageNumber + ".pdf" + ".zip");

                                            }
                                        }
                                    }
                                    else
                                    {
                                        memorystreamOut = memorystreamIn;

                                        memorystreamOut.Position = 0;
                                        System.IO.File.WriteAllBytes(Path.GetFileNameWithoutExtension(file) + "-" + r.GetString("page") + mailAttachment.PageNumber + ".pdf", memorystreamOut.ToArray());

                                        neueMail.Attachments.AddFileAttachment(Path.GetFileNameWithoutExtension(file) + "-" + r.GetString("page") + mailAttachment.PageNumber + ".pdf");
                                    }
                                }
                                catch (Exception)
                                {
                                    if (onlyReadyForTakOffMails.ToList()[z].EncryptAttachment)
                                    {
                                        using (ZipFile zip = new ZipFile())
                                        {
                                            MemoryStream ms = new MemoryStream();
                                            zip.Password = onlyReadyForTakOffMails.ToList()[z].Password;

                                            zip.AddFile(mailAttachment.File, "/"); // unencrypted

                                            //zip.Encryption = EncryptionAlgorithm.WinZipAes256;
                                            zip.Save(ms);
                                            
                                            ms.Position = 0;
                                            System.IO.File.WriteAllBytes(Path.GetFileNameWithoutExtension(Path.GetFileName(mailAttachment.File) + ".zip") + "-" + r.GetString("page") + mailAttachment.PageNumber + ".pdf", memorystreamOut.ToArray());

                                            neueMail.Attachments.AddFileAttachment(Path.GetFileNameWithoutExtension(Path.GetFileName(mailAttachment.File) + ".zip") + "-" + r.GetString("page") + mailAttachment.PageNumber + ".pdf");
                                        }
                                    }
                                    else
                                    {
                                        neueMail.Attachments.AddFileAttachment(mailAttachment.File);
                                    }
                                }
                            }

                            // Speicherung im ENTWÜRFE-Ordner

                            neueMail.Save(WellKnownFolderName.Drafts);
                        }
                        else
                        {
                            MailMessage neueMail = new MailMessage();
                            neueMail.IsBodyHtml = true;
                            neueMail.From = new MailAddress(Global.SmtpUser);
                            neueMail.To.Add(new MailAddress(onlyReadyForTakOffMails.ToList()[z].Recipient));
                            neueMail.Bcc.Add(new MailAddress(Global.SmtpUser));
                                                        
                            neueMail.Subject = onlyReadyForTakOffMails.ToList()[z].Subject;
                            neueMail.Body = body;

                            foreach (var mailAttachment in onlyReadyForTakOffMails.ToList()[z].mailAttachments)
                            {
                                try
                                {
                                    string file = openFileDialog.FileNames[mailAttachment.FileNumber - 1].ToString();
                                    int pageNumber = mailAttachment.PageNumber;

                                    memorystreamIn = new MemoryStream(ExtractPages(file, pageNumber, pageNumber));

                                    // If encryption is wanted ...

                                    if (onlyReadyForTakOffMails.ToList()[z].EncryptAttachment)
                                    {
                                        // Wenn ein GPG-Key vorhanden ist, wird mit ihm verschlüsselt.
                                        // Nachrangig wird symmetrisch verschlüsselt.

                                        if (onlyReadyForTakOffMails.ToList()[z].PublicKey != null)
                                        {
                                            string datei = "U:\\" + Path.GetFileNameWithoutExtension(file) + "-" + r.GetString("page") + mailAttachment.PageNumber + ".pdf";

                                            FileStream fileStream = new FileStream(datei, FileMode.Create, FileAccess.Write);
                                            memorystreamIn.WriteTo(fileStream);
                                            fileStream.Close();

                                            var encryptArg = new FileDataInput();

                                            encryptArg.Armor = true;
                                            encryptArg.InputFile = datei;
                                            encryptArg.OutputFile = "U:\\" + Path.GetFileNameWithoutExtension(file) + "-" + r.GetString("page") + mailAttachment.PageNumber + ".pdf" + ".pgp";
                                            encryptArg.Operation = DataOperation.Encrypt;
                                            encryptArg.Recipient = onlyReadyForTakOffMails.ToList()[z].PublicKey.Id;

                                            tool.ProcessData(encryptArg);
                                            neueMail.Attachments.Add(new System.Net.Mail.Attachment("U:\\" + Path.GetFileNameWithoutExtension(file) + "-" + r.GetString("page") + mailAttachment.PageNumber + ".pdf" + ".pgp"));
                                        }
                                        else
                                        {
                                            using (ZipFile zip = new ZipFile())
                                            {
                                                // http://shahvaibhav.com/create-zip-file-in-memory-using-dotnetzip/

                                                MemoryStream ms = new MemoryStream();

                                                // ... a password is been set to elstablish encryption.

                                                zip.Password = onlyReadyForTakOffMails.ToList()[z].Password;

                                                // 0 means no compression

                                                zip.CompressionLevel = 0;

                                                // Unencrypted memorystream is added to zip.

                                                zip.AddEntry(Path.GetFileName(file), memorystreamIn);
                                                //zip.AddEntry(file, memorystreamIn);

                                                // Alternative algorithm may not work with standard zip app.

                                                // zip.Encryption = EncryptionAlgorithm.WinZipAes256;

                                                zip.Save(ms);
                                                ms.Position = 0;
                                                neueMail.Attachments.Add(new System.Net.Mail.Attachment(ms, new System.Net.Mime.ContentType("application/zip")) { Name = Path.GetFileNameWithoutExtension(file) + "-" + r.GetString("page") + mailAttachment.PageNumber + ".pdf" + ".zip" });

                                            }
                                        }
                                    }
                                    else
                                    {
                                        memorystreamOut = memorystreamIn;
                                        neueMail.Attachments.Add(new System.Net.Mail.Attachment(memorystreamOut, Path.GetFileNameWithoutExtension(file) + "-" + r.GetString("page") + mailAttachment.PageNumber + ".pdf", "application/pdf"));

                                    }
                                }
                                catch (Exception)
                                {
                                    if (onlyReadyForTakOffMails.ToList()[z].EncryptAttachment)
                                    {
                                        using (ZipFile zip = new ZipFile())
                                        {
                                            MemoryStream ms = new MemoryStream();
                                            zip.Password = onlyReadyForTakOffMails.ToList()[z].Password;

                                            zip.AddFile(mailAttachment.File, "/"); // unencrypted

                                            //zip.Encryption = EncryptionAlgorithm.WinZipAes256;
                                            zip.Save(ms);
                                            ms.Position = 0;
                                            neueMail.Attachments.Add(new System.Net.Mail.Attachment(ms, new System.Net.Mime.ContentType("application/zip")) { Name = Path.GetFileName(mailAttachment.File) + ".zip" });
                                        }
                                    }
                                    else
                                    {
                                        neueMail.Attachments.Add(new System.Net.Mail.Attachment((mailAttachment.File)));
                                    }
                                }
                            }

                            SmtpClient client;


                            client = new SmtpClient(tbxSmtpServer.Text, Convert.ToInt32(tbxSmtpPort.Text));
                            client.Timeout = 7000;

                            System.Net.NetworkCredential nc = new System.Net.NetworkCredential(tbxSmtpUserMail.Text, tbxSmtpPassword.Text);
                            client.Credentials = nc;
                            client.EnableSsl = chkSsl.Checked;

                            if (onlyReadyForTakOffMails.ToList()[z].ReadyForTakeOff)
                            {
                                // Mach et Otze...

                                //if (neueMail.To.ToString() == "stefan.baeumer@berufskolleg-borken.de")
                                if (true)
                                {
                                    client.Send(neueMail);
                                }

                                logs.Add(new Log(r.GetString("Sent") + (z + 1).ToString("000") + ") " + onlyReadyForTakOffMails.ToList()[z].Recipient + "\t" + onlyReadyForTakOffMails.ToList()[z].Subject.Substring(0, Math.Min(onlyReadyForTakOffMails.ToList()[z].Subject.Length, 15)) + "... \t" + onlyReadyForTakOffMails.ToList()[z].Body.Substring(0, Math.Min(onlyReadyForTakOffMails.ToList()[z].Body.Length, 15)) + "... Pass: " + onlyReadyForTakOffMails.ToList()[z].Password + " \t"));
                            }
                            else
                            {
                                logs.Add(new Log(r.GetString("notSent") + (z + 1).ToString("000") + ") " + onlyReadyForTakOffMails.ToList()[z].Recipient + "\t" + onlyReadyForTakOffMails.ToList()[z].Subject.Substring(0, Math.Min(onlyReadyForTakOffMails.ToList()[z].Subject.Length, 15)) + "... \t" + onlyReadyForTakOffMails.ToList()[z].Body.Substring(0, Math.Min(onlyReadyForTakOffMails.ToList()[z].Body.Length, 15)) + "... Pass: " + onlyReadyForTakOffMails.ToList()[z].Password + " \t"));
                            }
                            memorystreamOut.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        logs.Add(new Log(r.GetString("error") + (z + 1).ToString("000") + ") " + onlyReadyForTakOffMails.ToList()[z].Recipient + "\t" + onlyReadyForTakOffMails.ToList()[z].Subject.Substring(0, Math.Min(onlyReadyForTakOffMails.ToList()[z].Subject.Length, 15)) + "... \t" + onlyReadyForTakOffMails.ToList()[z].Body.Substring(0, Math.Min(onlyReadyForTakOffMails.ToList()[z].Body.Length, 15)) + "... \t"));
                        logs.Add(new Log(ex.ToString()));
                        MessageBox.Show(ex.ToString());
                    }
                    finally
                    {
                        foreach (var attachment in onlyReadyForTakOffMails.ToList()[z].mailAttachments)
                        {
                            try
                            {
                                logs.Add(new Log("              " + r.GetString("file") + ": " + attachment.File.ToString()));
                            }
                            catch (Exception)
                            {
                                logs.Add(new Log("              " + r.GetString("file") + ": " + openFileDialog.FileNames[attachment.FileNumber-1] + " " + r.GetString("page") + ": " + attachment.PageNumber.ToString()));
                            }
                        }
                    }
                    z++;
                }
                try
                {
                    //onlyReadyForTakOffMails.ToList()[z - 1].Success = onlyReadyForTakOffMails.ToList()[z - 1].Success;
                    sendMailWorker.ReportProgress(prozentBeendet, onlyReadyForTakOffMails.ToList()[z - 1]);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
                
                System.Threading.Thread.Sleep(200);
                prozentBeendet++;
            }
            e.Result = prozentBeendet;
        }

        //public static string Decrypt(string cipherText, string passPhrase)
        //{
        //    byte[] cipherTextBytes = Convert.FromBase64String(cipherText);
        //    using (PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, null))
        //    {
        //        byte[] keyBytes = password.GetBytes(keysize / 8);
        //        using (RijndaelManaged symmetricKey = new RijndaelManaged())
        //        {
        //            symmetricKey.Mode = CipherMode.CBC;
        //            using (ICryptoTransform decryptor = symmetricKey.CreateDecryptor(keyBytes, initVectorBytes))
        //            {
        //                using (MemoryStream memoryStream = new MemoryStream(cipherTextBytes))
        //                {
        //                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read))
        //                    {
        //                        byte[] plainTextBytes = new byte[cipherTextBytes.Length];
        //                        int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
        //                        return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount);
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}

        //public MemoryStream CreateToMemoryStream(MemoryStream memStreamIn, string zipEntryName)
        //{

        //    MemoryStream outputMemStream = new MemoryStream();
        //    ZipOutputStream zipStream = new ZipOutputStream(outputMemStream);

        //    zipStream.SetLevel(3); //0-9, 9 being the highest level of compression

        //    ZipEntry newEntry = new ZipEntry(zipEntryName);
        //    newEntry.DateTime = DateTime.Now;

        //    zipStream.PutNextEntry(newEntry);

        //    StreamUtils.Copy(memStreamIn, zipStream, new byte[4096]);
        //    zipStream.CloseEntry();

        //    zipStream.IsStreamOwner = false;    // False stops the Close also Closing the underlying stream.
        //    zipStream.Close();          // Must finish the ZipOutputStream before using outputMemStream.

        //    outputMemStream.Position = 0;
        //    return outputMemStream;

        //    // Alternative outputs:
        //    // ToArray is the cleaner and easiest to use correctly with the penalty of duplicating allocated memory.
        //    //byte[] byteArrayOut = outputMemStream.ToArray();

        //    // GetBuffer returns a raw buffer raw and so you need to account for the true length yourself.
        //    //byte[] byteArrayOut = outputMemStream.GetBuffer();
        //    //long len = outputMemStream.Length;
        //}

        void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (Convert.ToInt32(e.Result) >= 100)
            {
                btnSendMails.Text = r.GetString("sendAgain");
                lblStatus.Text = r.GetString("FinishedSending");
                tbxSubject.Text = Global.Subject;
                tbxBody.Text = Global.Body;
                z = 0;
            }
            else
            {
                btnSendMails.Text = r.GetString("goOnAfter") + " " +  mails[z - 1].Recipient + " (" + e.Result.ToString() + "%)";
            }
            OpenLogFile();
        }

        private void OpenLogFile()
        {
            try
            {
                string homeDrive = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                string filename = homeDrive + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "SBM-Log" + ".txt";
                FileStream fileStream = new FileStream(filename, FileMode.Create);
                StreamWriter writer = new StreamWriter(fileStream);

                foreach (var log in logs)
                {
                    writer.WriteLine(log.Timestamp + " " + log.Record);    
                }
                
                writer.Close();
                Process.Start("notepad.exe", filename);
            }
            catch (Exception)
            {
            }
        }

        private string SearchAndReplace(string pInput, string pRecipient)
        {
            string output = pInput;

            // If expressions exists in input ...

            Regex aliasRegex = new Regex(@"\[\[(.*?)\]\]", RegexOptions.IgnoreCase);

            MatchCollection aliasMatches = aliasRegex.Matches(pInput);

            foreach (Match aliasMatch in aliasMatches)
            {
                output = output.Replace(aliasMatch.ToString(), aliases.SearchAndReturnAlias(aliasMatch.ToString(), pRecipient));
            }
            return output;
        }
              
        private void btnCSV_Click(object sender, EventArgs e)
        {
            progressBar.Value = progressBar.Minimum;
            lblStatusPercent.Text = "";
            
            browseIndex = -1;
            Global.SelectedColumn = -1;

            try
            {
                aliases.Clear();
                openFileDialog2.Filter = r.GetString("CsvFiles") + " (.csv)|*.csv";
                DialogResult result = openFileDialog2.ShowDialog();
                csvFile = openFileDialog2.FileName;
                
                if (csvFile.ToLower().EndsWith(".csv") && result == DialogResult.OK)
                {
                    mails = new Mails();
                    filteredMails = new Mails();
                    String[] rows = File.ReadAllText(csvFile).Split('\n');
                    aliases.AddAliasesFromCsv(rows);
                    Global.CsvValid = true;
                }
                else
                {
                    Global.CsvValid = false;
                    Properties.Settings.Default.aliasFile = "";
                }

                imgCsvSave.Visible = true;

                if (Global.PdfSuccessfullyReadIn)
                {
                    mails = mails.AddMails(openFileDialog.FileNames, aliases);
                    filteredMails = mails;
                }

                RenderCsvGroupBox();
                RenderPdfGroupBox();
                RenderMailGroupBox();
                RenderSendMail();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        List<string> exampleCsvFile = new List<string>();

        private void RenderCsvGroupBox()
        {
            if (Global.CsvValid)
            {
                if (Path.GetFileName(openFileDialog2.FileName.ToString()) != "")
                {
                    btnCSV.Text = Path.GetFileName(openFileDialog2.FileName.ToString());
                }
                else
                {
                    btnCSV.Text = r.GetString("ReadFromFormerSession");
                }
                
                imgCsvAttention.Visible = false;
                imgCsvOk.Visible = true;
                groupBoxCsvMain.Height = 126;
                groupBoxCsv.Visible = true;
                lblCsv.Visible = true;
                lblCsv.Text = (aliases.Count() - 1) + " " + r.GetString("RowsHaveBeenRead");
                groupBoxCsv.Height = 39;
                lblCsv.Height = 20;
                toolTip1.SetToolTip(lblCsv, aliases.TooltipString(aliases));
            }
            else
            {
                imgCsvOk.Visible = false;
                imgCsvAttention.Visible = false;
                groupBoxCsvMain.Height = 89;
                groupBoxCsv.Visible = false;
                lblCsv.Visible = false;
                btnCSV.Text = r.GetString("NoCsvFileHasBeenUploadedYet");
                                
                if (Global.PdfSuccessfullyReadIn)
                {
                    this.SearchForPlaceholders();   
                }
            }
        }
         
	    protected ArrayList SearchForMailAddressesAndConvertToLower(string pdfText)
	    {
		    ArrayList recipients = new ArrayList ();

            Regex regex = new Regex(@"(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
           + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
             + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
           + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})");
            
		    MatchCollection mailMatches = regex.Matches(pdfText);

		    foreach (Match mailMatch in mailMatches)
		    {
			    recipients.Add(mailMatch.Value.ToString().ToLower());
		    }
		    
		    return recipients;
	    }

	    public static byte[] ExtractPages(string sourcePdfPath, int startPage, int endPage)
	    {
		    PdfReader reader = null;
		    Document sourceDocument = null;
		    PdfCopy pdfCopyProvider = null;
		    PdfImportedPage importedPage = null;
		    MemoryStream target = new MemoryStream();

		    reader = new PdfReader(sourcePdfPath);
		    sourceDocument = new Document(reader.GetPageSizeWithRotation(startPage));
		    pdfCopyProvider = new PdfCopy(sourceDocument, target);
		    sourceDocument.Open();

		    for(int i = startPage; i <= endPage; i++)
		    {
			    importedPage = pdfCopyProvider.GetImportedPage(reader, i);
			    pdfCopyProvider.AddPage(importedPage);
		    }
		    sourceDocument.Close();
		    reader.Close();

		    return target.ToArray();
	    }
	    
	    protected void SmtpServer_Changed (object sender, EventArgs e)
	    {
            imgSmtpServerSave.Visible = true;
            Global.SmtpServer = tbxSmtpServer.Text;
            tbxSmtp_Change(sender, e);
	    }

	    protected void SmtpPort_Changed (object sender, EventArgs e)
	    {
		    int o;
            if (int.TryParse(tbxSmtpPort.Text, out o) && Convert.ToInt32(tbxSmtpPort.Text) <= 65535)
		    {
			    Global.SmtpPort = tbxSmtpPort.Text;
                imgSmtpPortSave.Visible = true;
                tbxSmtp_Change(sender, e);
		    } 
		    else
		    {
			    Global.MailSettingsValid = false;
                logs.Add(new Log(r.GetString("WrongPortNumber")));
                imgSmtpNotOk.Visible = true;
                if (tbxSmtpPort.Text != "")
                {
                    imgSmtpPortSave.Visible = false;    
                }
                lblSmtp.Text = r.GetString("FillInMaxFiveDigits");
		    }
	    }

	    protected void SmtpUser_Changed (object sender, EventArgs e)
	    {
            // Check if input is valid mail-address.

		    try {
			    
                var addr = new System.Net.Mail.MailAddress(tbxSmtpUserMail.Text);
                if (addr.Address == tbxSmtpUserMail.Text)
                {
                    if (tbxSmtpUserMail.Text.Contains("gmail.com"))
                    {
                        if (tbxSmtpPort.Text == "")
                        {
                            tbxSmtpPort.Text = "25";    
                        }
                        if (tbxSmtpPort.Text == "")
                        {
                            tbxSmtpServer.Text = "smtp.gmail.com";    
                        }
                        if (!chkSsl.Checked)
                        {
                            chkSsl.Checked = true;
                        }
                    }
                    if (tbxSmtpUserMail.Text.Contains("web.de"))
                    {
                        if (tbxSmtpPort.Text == "")
                        {
                            tbxSmtpPort.Text = "587";
                        }
                        if (tbxSmtpPort.Text == "")
                        {
                            tbxSmtpServer.Text = "smtp.web.de";
                        }
                        if (!chkSsl.Checked)
                        {
                            chkSsl.Checked = true;
                        }
                    }
                    
                    tbxSmtp_Change(sender, e);
                    imgSmtpMailaddressSave.Visible = true;
                    Global.SmtpUser = tbxSmtpUserMail.Text;
                }
		    }
		    catch 
		    {
                lblSmtp.Text = r.GetString("FillInCompleteMailAddress");
			    Global.MailSettingsValid = false;
		    }
	    }

	    protected void tbxPasswort_Changed(object sender, EventArgs e)
	    {
            imgSmtpPasswordSave.Visible = true;
            Global.SmtpPassword = tbxSmtpPassword.Text;
            tbxSmtp_Change(sender, e);
	    }


        void worker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                string smtpUserMail = tbxSmtpUserMail.Text;
                string password = tbxSmtpPassword.Text;
                int port = Convert.ToInt32(tbxSmtpPort.Text);
                string smtpServer = tbxSmtpServer.Text;
                bool sslEnabled = chkSsl.Checked;


                SmtpClient smtpClient = new SmtpClient();
                NetworkCredential basicCredential = new NetworkCredential(smtpUserMail, password);
                MailMessage message = new MailMessage();
                MailAddress fromAddress = new MailAddress(smtpUserMail);

                smtpClient.Host = smtpServer;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = basicCredential;
                smtpClient.EnableSsl = sslEnabled;

                message.From = fromAddress;
                message.Subject = r.GetString("testmailSubject");
                //Set IsBodyHtml to true means you can send HTML email.
                message.IsBodyHtml = true;
                message.Body = r.GetString("testmailBody");
                message.To.Add(smtpUserMail); 

                smtpClient.Send(message);
                Global.MailSettingsValid = true;

                // As connection is established, former errors got deleted.

                logs.RemoveAll(log => log.Record.Contains("System.Net.Mail.SmtpException"));
            }
            catch (Exception ex)
            {
                Global.MailSettingsValid = false;
                e.Result = ex.Message.ToString();
            }
        }

        void worker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
        }

        void worker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Result != null)
            {
                string tooltip = "";

                if (e.Result.ToString().PadLeft(4) == "Time")
                {
                    tooltip = e.Result.ToString() + "\nEin TimeOut kann an der Firewall liegen. Prüfen Sie das!";
                }

                toolTip1.SetToolTip(lblSmtp, tooltip);
                lblSmtp.Text = e.Result.ToString();
                imgSmtpOk.Visible = false;
                imgSmtpNotOk.Visible = true;
            }
            else
            {
                toolTip1.SetToolTip(lblSmtp, "");
                lblSmtp.Text = r.GetString("SettingsAreCorrect");
                logs.Add(new Log(r.GetString("TestmailSuccessfullySent")));
                imgSmtpOk.Visible = true;
                imgSmtpNotOk.Visible = false;
                Global.SmtpServer = tbxSmtpServer.Text;
                Global.SmtpPort = tbxSmtpPort.Text;
                Global.SmtpUser = tbxSmtpUserMail.Text;
            }
            RenderSendMail();
        }

        private void tbxSmtp_Change(object sender, EventArgs e)
        {   
            // If all textboxes are filled ...
                        
            groupBoxSmtpOk.Visible = true;
            groupBoxSmtpMain.Height = 243;

            Global.MailSettingsValid = false;
            lblSmtp.Text = r.GetString("smtpServerSettingsIncomple");
            imgSmtpNotOk.Visible = true;
            toolTip1.SetToolTip(lblSmtp, "Die Prüfung der Einstellungen läuft.");
            lblSmtp.Text = r.GetString("verifying");


            try
            {
                if (smtpWorker.IsBusy)
                {
                    smtpWorker.CancelAsync();
                }
                else
                {
                    imgSmtpOk.Visible = false;
                    imgSmtpNotOk.Visible = true;
                    toolTip1.SetToolTip(lblSmtp, "Die Prüfung der Einstellungen läuft.");
                    lblSmtp.Text = r.GetString("verifying");
                    smtpWorker.RunWorkerAsync();
                }

            }
            catch (Exception ex)
            {
                //toolTip1.SetToolTip(lblSmtp, ex.ToString());
            }
        }


        private void btnShowLog_Click(object sender, EventArgs e)
        {
            logs.Export();
        }
               
        private void imgMailSubjectSave_Click(object sender, EventArgs e)
        {
            Global.Subject = tbxSubject.Text;
            Properties.Settings.Default.Subject = tbxSubject.Text;
            Properties.Settings.Default.Save();
            imgMailSubjectSave.Visible = false;
        }


        internal void TextChangeByUser()
        {
            // If Event is fired by user directly and not by any method

            if (browseIndex > -1)
            {
                // mails that are change become ready for takeoff

                if (tbxBody.Text != "" && tbxSubject.Text != "")
                {
                    filteredMails[browseIndex].ReadyForTakeOff = true;
                }
                else
                {
                    filteredMails[browseIndex].ReadyForTakeOff = false;
                }

                string subject = tbxSubject.Text;
                string body = tbxBody.Text;

                // Aliases are changed on the fly and returned to textbox.

                if (Global.CsvValid)
                {
                    subject = SearchAndReplace(subject, filteredMails[browseIndex].Recipient);

                    string bodyBefore = body;
                    string subjectBefore = subject;

                    body = SearchAndReplace(body, filteredMails[browseIndex].Recipient);

                    // Avoid cursor jump to position 1

                    if (bodyBefore != body)
                    {
                        tbxBody.Select(tbxBody.Text.Length, 0);
                        tbxBody.Text = body;
                    }

                    if (subjectBefore != subject)
                    {
                        tbxSubject.Select(tbxSubject.Text.Length, 0);
                        tbxSubject.Text = subject;
                    }
                }


                filteredMails[browseIndex].Subject = subject;
                filteredMails[browseIndex].Body = body;

                imgMailSubjectSave.Visible = false;
                imgMailBodySave.Visible = false;
            }

            // ... If browseIndex equals -1, textchanged-event has effect on all mails.

            if (browseIndex == -1)
            {
                if (Global.Subject != tbxSubject.Text)
                {
                    // If user changes template subject mnually ...

                    if (tbxSubjectEnter)
                    {
                        // ... all subjects are changed. Placeholders get substituted. 

                        foreach (var mail in filteredMails)
                        {
                            if (mail.ReadyForTakeOff)
                            {
                                string subject = tbxSubject.Text;

                                if (Global.CsvValid)
                                {
                                    subject = SearchAndReplace(subject, mail.Recipient);
                                }

                                mail.Subject = subject;
                            }
                        }   
                    }
                    Global.Subject = tbxSubject.Text;
                }
                if (Global.Body != tbxBody.Text)
                {
                    // If user changes template body manually ...

                    if (tbxBodyEnter)
                    {
                        // ... all bodies are changed. Placeholders get substituted. 

                        foreach (var mail in filteredMails)
                        {
                            if (mail.ReadyForTakeOff)
                            {
                                string body = tbxBody.Text;

                                if (Global.CsvValid)
                                {
                                    body = SearchAndReplace(body, mail.Recipient);
                                }

                                mail.Body = body;
                            }
                        }
                        Global.Body = tbxBody.Text;   
                    }
                }
            }
            

            // If browseIndex is greater than -1 and mail is ready for takeoff, then the textchanged-event changes subject of one mail.

            
            RenderCsvGroupBox();
            RenderMailGroupBox();
        }

        private void tbxSubject_TextChanged(object sender, EventArgs e)
        {
            if (tbxSubjectEnter)
            {

                // If Event is fired by user directly and not by any method

                if (browseIndex > -1)
                {
                    // mails that are change become ready for takeoff

                    if (tbxSubject.Text != "" && tbxSubject.Text != "")
                    {
                        mails[browseIndex].ReadyForTakeOff = true;
                    }
                    else
                    {
                        mails[browseIndex].ReadyForTakeOff = false;
                    }

                    string Subject = tbxSubject.Text;

                    // Aliases are changed on the fly and returned to textbox.

                    if (Global.CsvValid)
                    {
                        string SubjectBefore = tbxSubject.Text;

                        Subject = SearchAndReplace(Subject, mails[browseIndex].Recipient);

                        // Avoid cursor jump to position 1

                        if (SubjectBefore != Subject)
                        {
                            tbxSubject.Select(tbxSubject.Text.Length, 0);
                            tbxSubject.Text = Subject;
                        }
                    }
                    else
                    {
                        this.SearchForPlaceholders();
                    }

                    mails[browseIndex].Subject = Subject;

                    imgMailSubjectSave.Visible = false;
                }

                // ... If browseIndex equals -1, textchanged-event has effect on all mails.

                if (browseIndex == -1)
                {
                    imgMailSubjectSave.Visible = true;

                    if (Global.Subject != tbxSubject.Text)
                    {
                        // ... all bodies are changed. Placeholders get substituted. 

                        foreach (var mail in mails)
                        {
                            if (mail.ReadyForTakeOff)
                            {
                                string Subject = tbxSubject.Text;

                                if (Global.CsvValid)
                                {
                                    Subject = SearchAndReplace(Subject, mail.Recipient);
                                }

                                mail.Subject = Subject;
                            }
                        }
                        Global.Subject = tbxSubject.Text;
                    }
                    if (!Global.CsvValid)
                    {
                        this.SearchForPlaceholders();
                    }
                }
                RenderSendMail();
            }
        }

        private void SearchForPlaceholders()
        {
            // If placeholder can be found in any body or subject a example for CSV-file is shown.
            // It's also offerd to generate CSV-file.

            Regex aliasRegex = new Regex(@"\[\[(.*?)\]\]", RegexOptions.IgnoreCase);

            MatchCollection aliasMatchesSubject1 = aliasRegex.Matches(tbxSubject.Text);
            MatchCollection aliasMatchesBody1 = aliasRegex.Matches(tbxBody.Text);

            IEnumerable<Match> aliasMatches = aliasMatchesSubject1.OfType<Match>().Concat(aliasMatchesBody1.OfType<Match>()).Where(m => m.Success);

            foreach (var mail in mails)
            {
                MatchCollection aliasMatchesSubject = aliasRegex.Matches(mail.Subject);
                MatchCollection aliasMatchesBody = aliasRegex.Matches(mail.Body);

                aliasMatches = aliasMatches.OfType<Match>().Union(aliasMatchesBody.OfType<Match>());
                aliasMatches = aliasMatches.OfType<Match>().Union(aliasMatchesSubject.OfType<Match>());
            }
            
            // Select Distinct for all matches.

            var uniqueMatches = aliasMatches .OfType<Match>() .Select(m => m.Value) .Distinct();

            // Create example Headrow.

            if (uniqueMatches.Count() > 0 && !Global.CsvValid && groupBoxMail.Visible)
            {
                string csvHead = "Mail";
                string csvFirstRow = "me@gmail.com";

                foreach (var match in uniqueMatches)
                {
                    csvHead = csvHead + ";" + match.ToString().Replace("[[", "").Replace("]]", "");
                    csvFirstRow = csvFirstRow + ";" + "Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa".Substring(0, (match.ToString().Length - 4));
                }

                groupBoxCsv.Visible = true;
                lblCsv.Visible = true;
                lblCsv.Height = 75;
                groupBoxCsv.Height = 93;
                groupBoxCsvMain.Height = 171;
                lblCsv.Text = r.GetString("PlaceholderHasBeenFoundNowUploadCsvWithHeadrow") + csvHead;
                toolTip1.SetToolTip(lblCsv, r.GetString("ClickToGenerateCsvFile"));
                exampleCsvFile.Clear();
                exampleCsvFile.Add(csvHead);
                exampleCsvFile.Add(csvFirstRow);
                imgCsvAttention.Visible = true;
                imgCsvOk.Visible = false;
            }
            else
            {
                imgCsvOk.Visible = false;
                imgCsvAttention.Visible = false;
                groupBoxCsvMain.Height = 89;
                groupBoxCsv.Visible = false;
                lblCsv.Visible = false;
            }
        }

        private void imgMailBodySave_Click(object sender, EventArgs e)
        {
            Global.Body = tbxBody.Text;
            Properties.Settings.Default.Body = tbxBody.Text;
            Properties.Settings.Default.Save();
            imgMailBodySave.Visible = false;
        }
        private void tbxBody_TextChanged(object sender, EventArgs e)
        {
            if (tbxBodyEnter)
            {
                if (browseIndex == -1)
                {
                    imgMailBodySave.Visible = true;
                }

                // If Event is fired by user directly and not by any method

                if (browseIndex > -1)
                {
                    // mails that are change become ready for takeoff

                    if (tbxBody.Text != "" && tbxSubject.Text != "")
                    {
                        mails[browseIndex].ReadyForTakeOff = true;
                    }
                    else
                    {
                        mails[browseIndex].ReadyForTakeOff = false;
                    }

                    string body = tbxBody.Text;

                    // Aliases are changed on the fly and returned to textbox.

                    if (Global.CsvValid)
                    {
                        string bodyBefore = tbxBody.Text;

                        body = SearchAndReplace(body, mails[browseIndex].Recipient);

                        // Avoid cursor jump to position 1

                        if (bodyBefore != body)
                        {
                            tbxBody.Select(tbxBody.Text.Length, 0);
                            tbxBody.Text = body;
                        }
                    }
                    else
                    {
                        this.SearchForPlaceholders();
                    }

                    mails[browseIndex].Body = body;

                    imgMailBodySave.Visible = false;
                }

                // ... If browseIndex equals -1, textchanged-event has effect on all mails.

                if (browseIndex == -1)
                {
                    imgMailBodySave.Visible = true;

                    if (Global.Body != tbxBody.Text)
                    {
                        // ... all bodies are changed. Placeholders get substituted. 

                        foreach (var mail in mails)
                        {
                            if (mail.ReadyForTakeOff)
                            {
                                string body = tbxBody.Text;

                                if (Global.CsvValid)
                                {
                                    body = SearchAndReplace(body, mail.Recipient);
                                }

                                mail.Body = body;
                            }
                        }
                        Global.Body = tbxBody.Text;
                    } 
                    
                    // If any placeholder in any mails exists ...
                    
                    if (!Global.CsvValid)
                    {
                        this.SearchForPlaceholders();
                    }
                }
            }
            //RenderSendMail();
        }

        private void imgBeforeMail_Click(object sender, EventArgs e)
        {  
            browseIndex--;

            if (filteredMails.Count > 0)
            {
                groupBoxMail.Visible = true;
                imgUploadFurtherAttachments.Visible = true;
                imgNextMail.Visible = true;
                imgMailSubjectSave.Visible = false;
                imgMailBodySave.Visible = false;

                toolTip1.SetToolTip(imgUploadFurtherAttachments, r.GetString("uploadFurtherAttachments"));
                toolTip1.SetToolTip(imgMailDeleted, r.GetString("MailDiscardedClickToActivateAgain"));
                toolTip1.SetToolTip(imgMailNotDeleted, r.GetString("ReadyForSendingClickToDiscrad"));
                toolTip1.SetToolTip(imgMailSubjectSave, r.GetString("OptionallySaveSubjectForFutureSessions"));
                toolTip1.SetToolTip(imgMailBodySave, r.GetString("OptionallySaveBodyForFutureSessions"));
                toolTip1.SetToolTip(imgBeforeMail, r.GetString("BackToMail") + " " + (browseIndex) + " " + r.GetString("of") + " " + filteredMails.Count());

                if (browseIndex == 0)
                {
                    toolTip1.SetToolTip(imgBeforeMail, r.GetString("BackToTemplateFullstop"));
                }

                if (browseIndex >= 0)
                {
                    toolTip1.SetToolTip(imgUploadFurtherAttachments, r.GetString("AddFurtherAttachmentToThisMailFullstop"));

                    tbxSubject.Text = filteredMails[browseIndex].Subject;
                    tbxBody.Text = filteredMails[browseIndex].Body;

                    if (filteredMails[browseIndex].ReadyForTakeOff)
                    {
                        imgMailDeleted.Visible = false;
                        imgMailNotDeleted.Visible = true;
                    }
                    else
                    {
                        imgMailDeleted.Visible = true;
                        imgMailNotDeleted.Visible = false;
                    }

                    if (filteredMails[browseIndex].EncryptAttachment)
                    {
                        imgEncryptAttachment.Visible = true;
                        
                        imgDecryptAttachment.Visible = false;

                        
                       
                    }
                    else
                    {
                        imgEncryptAttachment.Visible = false;
                       

                        imgDecryptAttachment.Visible = true;

                        toolTip1.SetToolTip(imgDecryptAttachment, r.GetString("TheAttachmentOfThisMailsWillBeSendNotEncrypted"));
                    }

                    if (browseIndex < filteredMails.Count - 1)
                    {
                        toolTip1.SetToolTip(imgNextMail, r.GetString("ForwardToMail") + " " + (browseIndex + 2) + " " + r.GetString("of") + " " + filteredMails.Count());
                    }
                    
                    string attachmentDeclined = r.GetString("attachment");

                    if (filteredMails[browseIndex].mailAttachments.Count > 1)
                    {
                        attachmentDeclined = r.GetString("attachments");
                    }

                    lblEmail.Text = "(" + r.GetString("Number") + (browseIndex + 1) + ") " + filteredMails[browseIndex].Recipient + " (" + filteredMails[browseIndex].mailAttachments.Count + " " + attachmentDeclined + ")";

                    // Enumerate attachments in tooltip.

                    string tooltip = "";

                    int z = 0;

                    foreach (var attachment in filteredMails[browseIndex].mailAttachments)
                    {
                        if (attachment.File != null)
                        {
                            tooltip = tooltip + Path.GetFileName(attachment.File) + "\n";
                        }
                        else
                        {
                            tooltip = tooltip + Path.GetFileName(openFileDialog.FileNames[filteredMails[browseIndex].mailAttachments[z].FileNumber - 1]) + r.GetString("commaPage") + filteredMails[browseIndex].mailAttachments[z].PageNumber + "\n";
                        }
                        z++;
                    }
                    toolTip1.SetToolTip(lblEmail, tooltip);
                }
                if (browseIndex < 0)
                {
                    tbxSubject.Text = Global.Subject;
                    tbxBody.Text = Global.Body;

                    imgMailDeleted.Visible = false;
                    imgMailNotDeleted.Visible = false;
                    imgBeforeMail.Visible = false;
                    imgNextMail.Focus();

                    if (Properties.Settings.Default.encryptAllAttachments)
                    {
                        imgDecryptAttachment.Visible = false;
                        imgEncryptAttachment.Visible = true;

                        toolTip1.SetToolTip(imgEncryptAttachment, r.GetString("AnAttemptIsBeingMadeToSendMailsEncrypted"));
                    }
                    else
                    {
                        imgDecryptAttachment.Visible = true;
                        imgEncryptAttachment.Visible = false;


                        toolTip1.SetToolTip(imgEncryptAttachment, r.GetString("EncryptionAssumesThatAPasswortExistsForThisRecipient"));
                    }

                    toolTip1.SetToolTip(imgUploadFurtherAttachments, r.GetString("AddToAll") + " " + filteredMails.Count + " " + r.GetString("MailsAFurtherAttachment"));
                    toolTip1.SetToolTip(imgNextMail, r.GetString("ForwardToMail") + " " + (browseIndex + 2) + " " + r.GetString("of") + " " + filteredMails.Count());
                    toolTip1.SetToolTip(lblEmail, "");
                    lblEmail.Text = r.GetString("YouSeeTheTemplateForAll");
                }
            }
            else
            {
                // If no mail could be read, hide groupBox.

                groupBoxMail.Visible = false;
            }
        }

        private void imgNextMail_Click(object sender, EventArgs e)
        {
            browseIndex++;

            if (filteredMails.Count > 0)
            {   
                imgUploadFurtherAttachments.Visible = true;
                imgBeforeMail.Visible = true;
                imgMailSubjectSave.Visible = false;
                imgMailBodySave.Visible = false;

                toolTip1.SetToolTip(imgUploadFurtherAttachments, r.GetString("uploadFurtherAttachments"));
                toolTip1.SetToolTip(imgMailDeleted, r.GetString("MailDiscardedClickToActivateAgain"));
                toolTip1.SetToolTip(imgMailSubjectSave, r.GetString("OptionallySaveSubjectForFutureSessions"));
                toolTip1.SetToolTip(imgMailBodySave, r.GetString("OptionallySaveBodyForFutureSessions"));
                toolTip1.SetToolTip(imgBeforeMail, r.GetString("BackToMail") + " " + (browseIndex) + " " + r.GetString("of") + " " + filteredMails.Count());

                if (browseIndex == 0)
                {
                    toolTip1.SetToolTip(imgBeforeMail, r.GetString("BackToTemplateFullstop"));
                }

                if (browseIndex >= 0 && browseIndex < filteredMails.Count())
                {
                    tbxSubject.Text = filteredMails[browseIndex].Subject;
                    tbxBody.Text = filteredMails[browseIndex].Body;

                    imgBeforeMail.Enabled = true;

                    toolTip1.SetToolTip(imgUploadFurtherAttachments, r.GetString("AddFurtherAttachmentToThisMailFullstop"));


                    if (filteredMails[browseIndex].ReadyForTakeOff)
                    {
                        imgMailDeleted.Visible = false;
                        imgMailNotDeleted.Visible = true;
                    }
                    else
                    {
                        imgMailDeleted.Visible = true;
                        imgMailNotDeleted.Visible = false;
                    }

                    if (filteredMails[browseIndex].EncryptAttachment)
                    {
                        imgEncryptAttachment.Visible = true;
                        
                        imgDecryptAttachment.Visible = false;

                        toolTip1.SetToolTip(imgEncryptAttachment, r.GetString("TheAttachmentsForTheRecipient") + " " + filteredMails[browseIndex].Recipient + r.GetString("willBeEncryptedFullstopTheRecipientHasToFillInPassword") + " " + filteredMails[browseIndex].Password + " " + r.GetString("toOpenAttachments"));
                    }
                    else
                    {
                        imgEncryptAttachment.Visible = false;
                        
                        imgDecryptAttachment.Visible = true;
                        toolTip1.SetToolTip(imgDecryptAttachment, r.GetString("AttachmentsWillBeSendNotEncrypted"));
                    }

                    if (browseIndex < filteredMails.Count - 1)
                    {
                        toolTip1.SetToolTip(imgNextMail, r.GetString("ForwardToMail") + " " + (browseIndex + 2) + " " + r.GetString("of") + " " + filteredMails.Count());
                    }
                    else
                    {
                        imgNextMail.Visible = false;
                    }

                    string attachmentDeclined = r.GetString("attachment");

                    if (mails[browseIndex].mailAttachments.Count > 1)
                    {
                        attachmentDeclined = r.GetString("attachments");
                    }
                    lblEmail.Text = "(" + r.GetString("Number") + (browseIndex + 1) + ") " + filteredMails[browseIndex].Recipient + " (" + filteredMails[browseIndex].mailAttachments.Count + " " + attachmentDeclined + ")";

                    // Enumerate attachments in tooltip.

                    string tooltip = "";

                    int z = 0;

                    foreach (var attachment in filteredMails[browseIndex].mailAttachments)
                    {
                        if (attachment.File != null)
                        {
                            tooltip = tooltip + Path.GetFileName(attachment.File) + "\n";
                        }
                        else
                        {
                            tooltip = tooltip + Path.GetFileName(openFileDialog.FileNames[filteredMails[browseIndex].mailAttachments[z].FileNumber - 1]) + r.GetString("commaPage") + filteredMails[browseIndex].mailAttachments[z].PageNumber + "\n";
                        }
                        z++;
                    }
                    toolTip1.SetToolTip(lblEmail, tooltip);
                }
            }
            else
            {
                // If no mail could be read, hide groupBox.

                groupBoxMail.Visible = false;
            }
        }

        private void RenderMailGroupBox()
        {
            // If PDF-file is not ok, nothing to show here.

            if (!Global.PdfSuccessfullyReadIn || Global.SelectedColumn == -1)
            {
                groupBoxMail.Visible = false;
            }

            if (Global.PdfSuccessfullyReadIn && Global.SelectedColumn > -1)
            {
                groupBoxMail.Visible = true;
                imgUploadFurtherAttachments.Visible = true;

                if (Properties.Settings.Default.encryptAllAttachments)
                {
                    imgEncryptAttachment.Visible = true;
                    imgDecryptAttachment.Visible = false;
                }
                else
                {
                    imgDecryptAttachment.Visible = true;
                    imgEncryptAttachment.Visible = false;
                }

                lblMailTitle.Text = r.GetString("CheckChangeDeletePreparedMails");
                lblEmail.Text = r.GetString("YouSeeTheTemplateForAll");

                imgMailSubjectSave.Visible = false;
                imgMailBodySave.Visible = false;
                
                toolTip1.SetToolTip(lblEmail, r.GetString("YouSeeTheTemplateForAll"));
                toolTip1.SetToolTip(imgUploadFurtherAttachments, r.GetString("uploadFurtherAttachments"));
                toolTip1.SetToolTip(imgMailDeleted, r.GetString("MailDiscardedClickToActivateAgain"));
                toolTip1.SetToolTip(imgMailNotDeleted, r.GetString("ReadyForSendingClickToDiscrad"));
                toolTip1.SetToolTip(imgMailSubjectSave, r.GetString("OptionallySaveSubjectForFutureSessions"));
                toolTip1.SetToolTip(imgMailBodySave, r.GetString("OptionallySaveBodyForFutureSessions"));
                toolTip1.SetToolTip(imgBeforeMail, r.GetString("BackToMail") + " " + (browseIndex) + " " + r.GetString("of") + " " + filteredMails.Count());
                            
                if (browseIndex == 0)
                {
                    toolTip1.SetToolTip(imgBeforeMail, r.GetString("BackToTemplateFullstop"));
                }

                if (browseIndex >= 0)
                {
                    imgBeforeMail.Enabled = true;

                    toolTip1.SetToolTip(imgUploadFurtherAttachments, r.GetString("AddFurtherAttachmentToThisMailFullstop"));

                    tbxSubject.Text = filteredMails[browseIndex].Subject;
                    tbxBody.Text = filteredMails[browseIndex].Body;

                    if (filteredMails[browseIndex].ReadyForTakeOff)
                    {
                        imgMailDeleted.Visible = false;
                        imgMailNotDeleted.Visible = true;
                    }
                    else
                    {
                        imgMailDeleted.Visible = true;
                        imgMailNotDeleted.Visible = false;
                    }

                    if (filteredMails[browseIndex].EncryptAttachment)
                    {
                        imgDecryptAttachment.Visible = false;
                        imgEncryptAttachment.Visible = true;
                    }
                    else
                    {
                        imgDecryptAttachment.Visible = true;
                        imgEncryptAttachment.Visible = false;
                    }

                    if (browseIndex < filteredMails.Count - 1)
                    {
                        toolTip1.SetToolTip(imgNextMail, r.GetString("ForwardToMail") + " " + (browseIndex + 2) + " " + r.GetString("of") + " " + filteredMails.Count());
                        imgNextMail.Visible = true;
                    }
                    
                    string attachmentDeclined = r.GetString("attachment");

                    if (filteredMails[browseIndex].mailAttachments.Count > 1)
                    {
                        attachmentDeclined = r.GetString("attachments");
                    }

                    lblEmail.Text = "(" + r.GetString("Number") + (browseIndex + 1) + ") " + filteredMails[browseIndex].Recipient + " (" + filteredMails[browseIndex].mailAttachments.Count + " " + attachmentDeclined + ")";

                    // Enumerate attachments in tooltip.

                    string tooltip = "";

                    int z = 0;

                    foreach (var attachment in filteredMails[browseIndex].mailAttachments)
                    {
                        if (!filteredMails[browseIndex].EncryptAttachment)
                        {
                            if (attachment.File != null)
                            {
                                tooltip = tooltip + Path.GetFileName(attachment.File) + "\n";
                            }
                            else
                            {
                                tooltip = tooltip + Path.GetFileName(openFileDialog.FileNames[filteredMails[browseIndex].mailAttachments[z].FileNumber - 1]) + r.GetString("commaPage") + filteredMails[browseIndex].mailAttachments[z].PageNumber + "\n";
                            }    
                        }
                        else
                        {
                            if (attachment.File != null)
                            {
                                tooltip = tooltip + Path.GetFileName(attachment.File) + ".zip (" + r.GetString("encrypted") + ")\n";
                            }
                            else
                            {
                                tooltip = tooltip + Path.GetFileName(openFileDialog.FileNames[filteredMails[browseIndex].mailAttachments[z].FileNumber - 1]) + r.GetString("commaPage") + filteredMails[browseIndex].mailAttachments[z].PageNumber + "\n";
                            }
                        }
                        
                        z++;
                    }
                    toolTip1.SetToolTip(lblEmail, tooltip);
                }
                if (browseIndex < 0)
                {
                    toolTip1.SetToolTip(imgUploadFurtherAttachments, r.GetString("AddToAll") + " " + filteredMails.Count + " " + r.GetString("MailsAFurtherAttachment"));
                    toolTip1.SetToolTip(imgNextMail, r.GetString("YouSeeTheTemplateForAll"));
                    tbxSubject.Text = Global.Subject;
                    tbxBody.Text = Global.Body;
                    imgBeforeMail.Enabled = false;
                    imgNextMail.Visible = true;
                    imgBeforeMail.Visible = false;
                    imgMailDeleted.Visible = false;
                    imgMailNotDeleted.Visible = false;
                    
                    if (Properties.Settings.Default.encryptAllAttachments)
                    {
                        imgEncryptAttachment.Visible = true;
                        imgDecryptAttachment.Visible = false;
                    }
                    else
                    {
                        imgDecryptAttachment.Visible = true;
                        imgEncryptAttachment.Visible = false;
                    }
                }   
            }
        }

        private void imgMailDeleted_Click(object sender, EventArgs e)
        {
            filteredMails[browseIndex].ReadyForTakeOff = true;
            imgMailNotDeleted.Visible = true;
            imgMailDeleted.Visible = false;
            RenderSendMail();
        }
        
        private void imgMailNotDeleted_Click(object sender, EventArgs e)
        {
            filteredMails[browseIndex].ReadyForTakeOff = false;
            imgMailNotDeleted.Visible = false;
            imgMailDeleted.Visible = true;
            RenderSendMail();
        }

        private void imgSmtpMailaddressSave_Click(object sender, EventArgs e)
        {
            imgSmtpMailaddressSave.Visible = false;
            Properties.Settings.Default.SmtpUser = tbxSmtpUserMail.Text;
            Properties.Settings.Default.Save();
        }

        private void imgSmtpServerSave_Click(object sender, EventArgs e)
        {
            imgSmtpServerSave.Visible = false;
            Properties.Settings.Default.SmtpServer = tbxSmtpServer.Text;
            Properties.Settings.Default.Save();
        }

        private void imgSmtpPortSave_Click(object sender, EventArgs e)
        {
            imgSmtpPortSave.Visible = false; ;
            Properties.Settings.Default.SmtpPort = tbxSmtpPort.Text;
            Properties.Settings.Default.Save();
        }

        private void imgSmtpPasswordSave_Click(object sender, EventArgs e)
        {
            imgSmtpPasswordSave.Visible = false;
            Properties.Settings.Default.SmtpPassword = tbxSmtpPassword.Text;
            Properties.Settings.Default.Save();
        }

        List<string> attachmentsForAllMails = new List<string>();

        private void UploadFurtherAttachments_Click(object sender, EventArgs e)
        {
            try
            {
                if (!filteredMails[browseIndex].ReadyForTakeOff)
                {
                    MessageBox.Show(r.GetString("noAdditionalAttachmentForDeletedMail"));
                    return;
                }
            }
            catch (Exception)
            {
                
            }
            
            
            OpenFileDialog openfiledialogOptionalAttachments = new OpenFileDialog();

            openfiledialogOptionalAttachments.Multiselect = true;
            openfiledialogOptionalAttachments.Title = r.GetString("OptionalAttachments");

            DialogResult result = openfiledialogOptionalAttachments.ShowDialog();

            string message = r.GetString("OptionalAttachments");

            if (browseIndex == -1)
            {
                message = message + "\n" + r.GetString("forAllMails");
                foreach (var mail in filteredMails)
                {
                    foreach (String file in openfiledialogOptionalAttachments.FileNames)
                    {
                        message = message + "\n" + file.ToString();
                        mail.mailAttachments.Add(new MailAttachment(file));
                    }
                }
                foreach (String file in openfiledialogOptionalAttachments.FileNames)
                {
                    attachmentsForAllMails.Add(file);
                }
            }
            else
            {
                if (filteredMails[browseIndex].ReadyForTakeOff)
                {
                    message = message + "\n" + r.GetString("forTheMailRecipient") + " " + filteredMails[browseIndex].Recipient + ": ";
                    foreach (String file in openfiledialogOptionalAttachments.FileNames)
                    {
                        message = message + "\n" + file;
                        filteredMails[browseIndex].mailAttachments.Add(new MailAttachment(file));
                    }    
                }
            }
            RenderMailGroupBox();
            RenderSendMail();
        }
        
        private void lblCsv_Click_1(object sender, EventArgs e)
        {
            // Create an example for Alias file.

            try
            {
                string homeDrive = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                string filename = homeDrive + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "SBM-Alias" + ".csv";
                FileStream fileStream = new FileStream(filename, FileMode.Create);
                StreamWriter writer = new StreamWriter(fileStream);
                writer.WriteLine(exampleCsvFile[0]);
                writer.WriteLine(exampleCsvFile[1]);
                writer.Close();
                Process.Start("notepad.exe", filename);   
            }
            catch (Exception)
            {
            }
        }

        private void pbCsvSave_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
            imgCsvSave.Visible = false;
        }

        private void tbxBody_Enter(object sender, EventArgs e)
        {
            tbxBodyEnter = true;
        }

        private void tbxBody_Leave(object sender, EventArgs e)
        {
            tbxBodyEnter = false;
        }

        private void tbxSubject_Enter(object sender, EventArgs e)
        {
            tbxSubjectEnter = true;
        }

        private void tbxSubject_Leave(object sender, EventArgs e)
        {
            tbxSubjectEnter = false;
        }

        private void imgSelectGerman_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.SelectedLanguage = "de-DE";
            Properties.Settings.Default.Save();
            this.settingLanguage();
        }

        private void imgSelectEnglish_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.SelectedLanguage = "en-UK";
            Properties.Settings.Default.Save();
            this.settingLanguage();
        }

        private void imgSelectSpanish_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not yet implemented. Maybe later ...");
            Properties.Settings.Default.SelectedLanguage = "es-ES";
            Properties.Settings.Default.Save();
            this.settingLanguage();
        }

        private void imgSelectNetherlands_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not yet implemented. Maybe later ...");
            Properties.Settings.Default.SelectedLanguage = "nl-NL";
            Properties.Settings.Default.Save();
            this.settingLanguage();
        }

        private void imgSelectFrench_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not yet implemented. Maybe later ...");
            Properties.Settings.Default.SelectedLanguage = "fr-FR";
            Properties.Settings.Default.Save();
            this.settingLanguage();
        }

        private void imgSelectTrurkey_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Not yet implemented. Maybe later ...");
            Properties.Settings.Default.SelectedLanguage = "tr-TR";
            Properties.Settings.Default.Save();
            this.settingLanguage();
        }

        private void imgDecryptAttachment_Click(object sender, EventArgs e)
        {
            string tooltip = "";

            //if (Global.CsvValid)
            //{
                if (browseIndex == -1)
                {
                    Properties.Settings.Default.encryptAllAttachments = true;
                    Properties.Settings.Default.Save();

                    int allMails = 0;
                    int encMails = 0;

                    foreach (var mail in filteredMails)
                    {
                        allMails++;
                    
                    var key = (from k in tool.ListKeys(KeyTarget.Public).ToList()
                              from x in k.UserIds
                              where x.Contains(mail.Recipient)
                              select k).FirstOrDefault();

                    // Falls es einen Key oder ein Passwort gibt, wird verschlüsselt ...

                    if (mail.Password != "" || key != null)
                    {
                        mail.EncryptAttachment = true;
                        encMails++;

                        if (key != null)
                            {
                                mail.PublicKey = key;
                            }
                        }
                    }

                    if (encMails == 0)
                    {
                        tooltip = r.GetString("NoEncryptionBecauseNoPassword");
                        imgDecryptAttachment.Visible = true;
                        imgEncryptAttachment.Visible = false;
                        toolTip1.SetToolTip(imgDecryptAttachment, tooltip);
                        MessageBox.Show(tooltip, r.GetString("Attention"));
                    }
                    if (encMails > 0 && encMails < allMails)
                    {
                        tooltip = r.GetString("TheAttachmentsFrom") + " " + encMails + " " + r.GetString("of") + " " + allMails + " " + r.GetString("MailsAreEncrypted") + " " + (allMails - encMails) + " " + r.GetString("RecipientsNoPasswordExists");
                        imgDecryptAttachment.Visible = false;
                        imgEncryptAttachment.Visible = true;    
                    }
                    if (encMails == allMails)
                    {
                        tooltip = r.GetString("All") + " " + allMails + " " + r.GetString("MailsAreEncryptedFullstop");
                        imgDecryptAttachment.Visible = false;
                        imgEncryptAttachment.Visible = true;    
                    }
                }
                else
                {
                    if (filteredMails[browseIndex].Password != "")
                    {
                        filteredMails[browseIndex].EncryptAttachment = true;
                        tooltip = r.GetString("TheAttachmentsForTheRecipient") + " " + filteredMails[browseIndex].Recipient + " " + r.GetString("areEncryptedFullstopTheRecipientHasToFillInThePassword") + filteredMails[browseIndex].Password + " " + r.GetString("toDecryptAttachments");
                        imgDecryptAttachment.Visible = false;
                        imgEncryptAttachment.Visible = true;
                    }
                    else
                    {
                        tooltip = r.GetString("TheAttemptToEncryptAttachmentsForTheRecipient") + " " + filteredMails[browseIndex].Recipient + r.GetString("hasFaildBecauseOfNoPasswordExistsInCsvFile") + " " + filteredMails[browseIndex].Recipient + ",Meyer,Pit,123";
                        imgDecryptAttachment.Visible = true;
                        imgEncryptAttachment.Visible = false;
                        toolTip1.SetToolTip(imgDecryptAttachment, tooltip);
                        MessageBox.Show(tooltip, r.GetString("Attention"));
                    }
                }
                
                toolTip1.SetToolTip(imgEncryptAttachment, tooltip);    
            //}
            //else
            //{
            //    MessageBox.Show( r.GetString("EncryptionAssumesThatAPasswortExistsForThisRecipient"));
            //}
        }

        private void imgEncryptAttachment_Click(object sender, EventArgs e)
        {
            string tooltip = "";

            if (browseIndex == -1)
            {
                Properties.Settings.Default.encryptAllAttachments = false;
                Properties.Settings.Default.Save();

                tooltip = r.GetString("AttachmentsWillNotBeEncryptedFullstopClickIfYouWantMoreSecurity");

                foreach (var mail in filteredMails)
                {
                    mail.EncryptAttachment = false;
                }
            }
            else
            {
                filteredMails[browseIndex].EncryptAttachment = false;
                tooltip = r.GetString("TheAttachmentOfThisMailWillBeSendDecryptedFullstop");
            }
            imgDecryptAttachment.Visible = true;
            imgEncryptAttachment.Visible = false;

            toolTip1.SetToolTip(imgDecryptAttachment, tooltip);
        }

        private void imgPrintPasswords_Click(object sender, EventArgs e)
        {
            string homeDrive = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            string filename = homeDrive + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "SBM-PW" + ".txt";
            FileStream fileStream = new FileStream(filename, FileMode.Create);
            StreamWriter writer = new StreamWriter(fileStream);

            writer.WriteLine("Drucken Sie die Liste aus und teilen Sie den Empfängern das Passwort mit.");
            writer.WriteLine("Die Passwörter bleibt für diese installierte Version stets identisch.");
            writer.WriteLine("Senden Sie Passörter niemals per E-Mail.");
            writer.WriteLine("");

            foreach (var mail in filteredMails)
            {
                writer.WriteLine(mail.Recipient + " " + mail.Password);
            }
           
            writer.Close();
            Process.Start("notepad.exe", filename);
        }

        private void imgInfo_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Thank you for using PDF-Bulk-Letter-Mailer.\nThis program comes to you free of charge and with absolute NO WARRENTY.\nRead the AGPL for further information.\nKeep in mind, that you are not allowed to use this program for contacting people that might not want to be contacted by you.\nThe same applies to people, you do not personally know.", "About");
        }

        private void chkSsl_CheckedChanged(object sender, EventArgs e)
        {
            imgSslSave.Visible = true;
            tbxSmtp_Change(sender, e);
        }

        private void imgSsl_Click(object sender, EventArgs e)
        {
            imgSslSave.Visible = false;
            Properties.Settings.Default.Ssl = chkSsl.Checked;
            Properties.Settings.Default.Save();
        }

        private void checkExchange_CheckedChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.exchange = checkExchange.Checked;
            Properties.Settings.Default.Save();

            if (checkExchange.Checked)
            {
                metroTextBox1.Visible = true;
            }
            else
            {
                metroTextBox1.Visible = false;               
            }
        }

        private void metroTextBox1_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.exchangeUrl = metroTextBox1.Text;
            Properties.Settings.Default.Save();
        }
    }
}
