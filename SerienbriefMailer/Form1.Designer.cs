﻿namespace SerienbriefMailer
{
    partial class frm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm));
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.lblStatus1 = new System.Windows.Forms.Label();
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.lblPdfTitle = new MetroFramework.Controls.MetroLabel();
            this.btnPdfFileSelect = new MetroFramework.Controls.MetroButton();
            this.lblPdfFile = new MetroFramework.Controls.MetroLabel();
            this.tbxSmtpUserMail = new MetroFramework.Controls.MetroTextBox();
            this.lblSmtpUserMail = new MetroFramework.Controls.MetroLabel();
            this.lblSmtpServer = new MetroFramework.Controls.MetroLabel();
            this.tbxSmtpServer = new MetroFramework.Controls.MetroTextBox();
            this.lblSmtpPort = new MetroFramework.Controls.MetroLabel();
            this.tbxSmtpPort = new MetroFramework.Controls.MetroTextBox();
            this.lblSmtpPassword = new MetroFramework.Controls.MetroLabel();
            this.tbxSmtpPassword = new MetroFramework.Controls.MetroTextBox();
            this.tbxSubject = new MetroFramework.Controls.MetroTextBox();
            this.tbxBody = new MetroFramework.Controls.MetroTextBox();
            this.lblSmtpTitle = new MetroFramework.Controls.MetroLabel();
            this.lblSmtp = new MetroFramework.Controls.MetroLabel();
            this.lblCsvTitle = new MetroFramework.Controls.MetroLabel();
            this.btnCSV = new MetroFramework.Controls.MetroButton();
            this.lblCsv = new MetroFramework.Controls.MetroLabel();
            this.btnSendMails = new MetroFramework.Controls.MetroButton();
            this.progressBar = new MetroFramework.Controls.MetroProgressBar();
            this.lblStatus = new MetroFramework.Controls.MetroLabel();
            this.lblMailTitle = new MetroFramework.Controls.MetroLabel();
            this.lblCopyleft = new MetroFramework.Controls.MetroLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.imgSelectTrurkey = new System.Windows.Forms.PictureBox();
            this.imgSelectFrench = new System.Windows.Forms.PictureBox();
            this.imgSelectNetherlands = new System.Windows.Forms.PictureBox();
            this.imgSelectSpanish = new System.Windows.Forms.PictureBox();
            this.imgSelectGerman = new System.Windows.Forms.PictureBox();
            this.imgSelectEnglish = new System.Windows.Forms.PictureBox();
            this.groupBoxPdfMail = new System.Windows.Forms.GroupBox();
            this.imgColumnDecision = new System.Windows.Forms.PictureBox();
            this.imgPdfAttention = new System.Windows.Forms.PictureBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.groupBoxCsv = new System.Windows.Forms.GroupBox();
            this.imgCsvOk = new System.Windows.Forms.PictureBox();
            this.imgCsvAttention = new System.Windows.Forms.PictureBox();
            this.groupBoxSmtpMain = new System.Windows.Forms.GroupBox();
            this.checkExchange = new System.Windows.Forms.CheckBox();
            this.imgSmtpServerSave = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkSsl = new System.Windows.Forms.CheckBox();
            this.imgSslSave = new System.Windows.Forms.PictureBox();
            this.groupBoxSmtpOk = new System.Windows.Forms.GroupBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.imgSmtpOk = new System.Windows.Forms.PictureBox();
            this.imgSmtpNotOk = new System.Windows.Forms.PictureBox();
            this.imgSmtpPasswordSave = new System.Windows.Forms.PictureBox();
            this.imgSmtpPortSave = new System.Windows.Forms.PictureBox();
            this.imgSmtpMailaddressSave = new System.Windows.Forms.PictureBox();
            this.groupBoxMail = new System.Windows.Forms.GroupBox();
            this.imgDecryptAttachment = new System.Windows.Forms.PictureBox();
            this.imgEncryptAttachment = new System.Windows.Forms.PictureBox();
            this.imgMailBodySave = new System.Windows.Forms.PictureBox();
            this.imgMailNotDeleted = new System.Windows.Forms.PictureBox();
            this.lblEmail = new MetroFramework.Controls.MetroLabel();
            this.imgNextMail = new System.Windows.Forms.PictureBox();
            this.imgUploadFurtherAttachments = new System.Windows.Forms.PictureBox();
            this.imgBeforeMail = new System.Windows.Forms.PictureBox();
            this.imgMailDeleted = new System.Windows.Forms.PictureBox();
            this.imgMailSubjectSave = new System.Windows.Forms.PictureBox();
            this.groupBoxPdf = new System.Windows.Forms.GroupBox();
            this.imgPdfOk = new System.Windows.Forms.PictureBox();
            this.imgPdfOkNot = new System.Windows.Forms.PictureBox();
            this.groupBoxCsvMain = new System.Windows.Forms.GroupBox();
            this.imgCsvSave = new System.Windows.Forms.PictureBox();
            this.groupBoxSendMail = new System.Windows.Forms.GroupBox();
            this.lblStatusPercent = new MetroFramework.Controls.MetroLabel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.imgInfo = new System.Windows.Forms.PictureBox();
            this.metroTextBox1 = new MetroFramework.Controls.MetroTextBox();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSelectTrurkey)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSelectFrench)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSelectNetherlands)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSelectSpanish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSelectGerman)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSelectEnglish)).BeginInit();
            this.groupBoxPdfMail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgColumnDecision)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPdfAttention)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBoxCsv.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCsvOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCsvAttention)).BeginInit();
            this.groupBoxSmtpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSmtpServerSave)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSslSave)).BeginInit();
            this.groupBoxSmtpOk.SuspendLayout();
            this.groupBox17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSmtpOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSmtpNotOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSmtpPasswordSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSmtpPortSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSmtpMailaddressSave)).BeginInit();
            this.groupBoxMail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgDecryptAttachment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEncryptAttachment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMailBodySave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMailNotDeleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNextMail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgUploadFurtherAttachments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBeforeMail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMailDeleted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMailSubjectSave)).BeginInit();
            this.groupBoxPdf.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPdfOk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPdfOkNot)).BeginInit();
            this.groupBoxCsvMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCsvSave)).BeginInit();
            this.groupBoxSendMail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // openFileDialog
            // 
            this.openFileDialog.Multiselect = true;
            // 
            // lblStatus1
            // 
            this.lblStatus1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatus1.Location = new System.Drawing.Point(12, 577);
            this.lblStatus1.Name = "lblStatus1";
            this.lblStatus1.Size = new System.Drawing.Size(389, 18);
            this.lblStatus1.TabIndex = 4;
            // 
            // lblPdfTitle
            // 
            this.lblPdfTitle.AutoSize = true;
            this.lblPdfTitle.Location = new System.Drawing.Point(8, 16);
            this.lblPdfTitle.Name = "lblPdfTitle";
            this.lblPdfTitle.Size = new System.Drawing.Size(99, 19);
            this.lblPdfTitle.TabIndex = 10;
            this.lblPdfTitle.Text = "PDF-File-Select";
            // 
            // btnPdfFileSelect
            // 
            this.btnPdfFileSelect.Location = new System.Drawing.Point(12, 37);
            this.btnPdfFileSelect.Name = "btnPdfFileSelect";
            this.btnPdfFileSelect.Size = new System.Drawing.Size(377, 39);
            this.btnPdfFileSelect.TabIndex = 0;
            this.btnPdfFileSelect.Text = "SelectPdf";
            this.btnPdfFileSelect.UseSelectable = true;
            this.btnPdfFileSelect.Click += new System.EventHandler(this.btnPdfFileSelected_Click);
            // 
            // lblPdfFile
            // 
            this.lblPdfFile.Location = new System.Drawing.Point(2, 12);
            this.lblPdfFile.Name = "lblPdfFile";
            this.lblPdfFile.Size = new System.Drawing.Size(345, 19);
            this.lblPdfFile.TabIndex = 12;
            this.lblPdfFile.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbxSmtpUserMail
            // 
            this.tbxSmtpUserMail.Lines = new string[0];
            this.tbxSmtpUserMail.Location = new System.Drawing.Point(108, 41);
            this.tbxSmtpUserMail.MaxLength = 32767;
            this.tbxSmtpUserMail.Name = "tbxSmtpUserMail";
            this.tbxSmtpUserMail.PasswordChar = '\0';
            this.tbxSmtpUserMail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxSmtpUserMail.SelectedText = "";
            this.tbxSmtpUserMail.Size = new System.Drawing.Size(280, 23);
            this.tbxSmtpUserMail.TabIndex = 0;
            this.tbxSmtpUserMail.UseSelectable = true;
            this.tbxSmtpUserMail.TextChanged += new System.EventHandler(this.SmtpUser_Changed);
            // 
            // lblSmtpUserMail
            // 
            this.lblSmtpUserMail.Location = new System.Drawing.Point(6, 43);
            this.lblSmtpUserMail.Name = "lblSmtpUserMail";
            this.lblSmtpUserMail.Size = new System.Drawing.Size(101, 20);
            this.lblSmtpUserMail.TabIndex = 14;
            this.lblSmtpUserMail.Text = "MailAddress";
            this.lblSmtpUserMail.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblSmtpServer
            // 
            this.lblSmtpServer.Location = new System.Drawing.Point(6, 76);
            this.lblSmtpServer.Name = "lblSmtpServer";
            this.lblSmtpServer.Size = new System.Drawing.Size(101, 19);
            this.lblSmtpServer.TabIndex = 16;
            this.lblSmtpServer.Text = "SMTP-Server";
            this.lblSmtpServer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbxSmtpServer
            // 
            this.tbxSmtpServer.Lines = new string[0];
            this.tbxSmtpServer.Location = new System.Drawing.Point(108, 74);
            this.tbxSmtpServer.MaxLength = 32767;
            this.tbxSmtpServer.Name = "tbxSmtpServer";
            this.tbxSmtpServer.PasswordChar = '\0';
            this.tbxSmtpServer.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxSmtpServer.SelectedText = "";
            this.tbxSmtpServer.Size = new System.Drawing.Size(280, 23);
            this.tbxSmtpServer.TabIndex = 1;
            this.tbxSmtpServer.UseSelectable = true;
            this.tbxSmtpServer.TextChanged += new System.EventHandler(this.SmtpServer_Changed);
            this.tbxSmtpServer.Leave += new System.EventHandler(this.tbxSmtp_Change);
            // 
            // lblSmtpPort
            // 
            this.lblSmtpPort.Location = new System.Drawing.Point(6, 110);
            this.lblSmtpPort.Name = "lblSmtpPort";
            this.lblSmtpPort.Size = new System.Drawing.Size(101, 19);
            this.lblSmtpPort.TabIndex = 18;
            this.lblSmtpPort.Text = "Port";
            this.lblSmtpPort.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbxSmtpPort
            // 
            this.tbxSmtpPort.Lines = new string[0];
            this.tbxSmtpPort.Location = new System.Drawing.Point(108, 107);
            this.tbxSmtpPort.MaxLength = 32767;
            this.tbxSmtpPort.Name = "tbxSmtpPort";
            this.tbxSmtpPort.PasswordChar = '\0';
            this.tbxSmtpPort.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxSmtpPort.SelectedText = "";
            this.tbxSmtpPort.Size = new System.Drawing.Size(172, 23);
            this.tbxSmtpPort.TabIndex = 2;
            this.tbxSmtpPort.UseSelectable = true;
            this.tbxSmtpPort.TextChanged += new System.EventHandler(this.SmtpPort_Changed);
            this.tbxSmtpPort.Leave += new System.EventHandler(this.tbxSmtp_Change);
            // 
            // lblSmtpPassword
            // 
            this.lblSmtpPassword.Location = new System.Drawing.Point(6, 141);
            this.lblSmtpPassword.Name = "lblSmtpPassword";
            this.lblSmtpPassword.Size = new System.Drawing.Size(101, 19);
            this.lblSmtpPassword.TabIndex = 20;
            this.lblSmtpPassword.Text = "Password";
            this.lblSmtpPassword.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbxSmtpPassword
            // 
            this.tbxSmtpPassword.Lines = new string[0];
            this.tbxSmtpPassword.Location = new System.Drawing.Point(108, 139);
            this.tbxSmtpPassword.MaxLength = 32767;
            this.tbxSmtpPassword.Name = "tbxSmtpPassword";
            this.tbxSmtpPassword.PasswordChar = '*';
            this.tbxSmtpPassword.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxSmtpPassword.SelectedText = "";
            this.tbxSmtpPassword.Size = new System.Drawing.Size(280, 23);
            this.tbxSmtpPassword.TabIndex = 3;
            this.tbxSmtpPassword.UseSelectable = true;
            this.tbxSmtpPassword.TextChanged += new System.EventHandler(this.tbxPasswort_Changed);
            // 
            // tbxSubject
            // 
            this.tbxSubject.Lines = new string[0];
            this.tbxSubject.Location = new System.Drawing.Point(13, 37);
            this.tbxSubject.MaxLength = 32767;
            this.tbxSubject.Name = "tbxSubject";
            this.tbxSubject.PasswordChar = '\0';
            this.tbxSubject.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxSubject.SelectedText = "";
            this.tbxSubject.Size = new System.Drawing.Size(376, 23);
            this.tbxSubject.TabIndex = 1;
            this.tbxSubject.UseSelectable = true;
            this.tbxSubject.TextChanged += new System.EventHandler(this.tbxSubject_TextChanged);
            this.tbxSubject.Enter += new System.EventHandler(this.tbxSubject_Enter);
            this.tbxSubject.Leave += new System.EventHandler(this.tbxSubject_Leave);
            // 
            // tbxBody
            // 
            this.tbxBody.Lines = new string[0];
            this.tbxBody.Location = new System.Drawing.Point(12, 66);
            this.tbxBody.MaxLength = 32767;
            this.tbxBody.Multiline = true;
            this.tbxBody.Name = "tbxBody";
            this.tbxBody.PasswordChar = '\0';
            this.tbxBody.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.tbxBody.SelectedText = "";
            this.tbxBody.Size = new System.Drawing.Size(377, 100);
            this.tbxBody.TabIndex = 2;
            this.tbxBody.UseSelectable = true;
            this.tbxBody.TextChanged += new System.EventHandler(this.tbxBody_TextChanged);
            this.tbxBody.Enter += new System.EventHandler(this.tbxBody_Enter);
            this.tbxBody.Leave += new System.EventHandler(this.tbxBody_Leave);
            // 
            // lblSmtpTitle
            // 
            this.lblSmtpTitle.AutoSize = true;
            this.lblSmtpTitle.Location = new System.Drawing.Point(9, 16);
            this.lblSmtpTitle.Name = "lblSmtpTitle";
            this.lblSmtpTitle.Size = new System.Drawing.Size(87, 19);
            this.lblSmtpTitle.TabIndex = 26;
            this.lblSmtpTitle.Text = "SMTP-Server";
            // 
            // lblSmtp
            // 
            this.lblSmtp.Location = new System.Drawing.Point(6, 12);
            this.lblSmtp.Name = "lblSmtp";
            this.lblSmtp.Size = new System.Drawing.Size(335, 20);
            this.lblSmtp.TabIndex = 27;
            this.lblSmtp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCsvTitle
            // 
            this.lblCsvTitle.AutoSize = true;
            this.lblCsvTitle.Location = new System.Drawing.Point(8, 15);
            this.lblCsvTitle.Name = "lblCsvTitle";
            this.lblCsvTitle.Size = new System.Drawing.Size(93, 19);
            this.lblCsvTitle.TabIndex = 28;
            this.lblCsvTitle.Text = "ChooseCsvFile";
            // 
            // btnCSV
            // 
            this.btnCSV.Location = new System.Drawing.Point(13, 36);
            this.btnCSV.Name = "btnCSV";
            this.btnCSV.Size = new System.Drawing.Size(376, 39);
            this.btnCSV.TabIndex = 0;
            this.btnCSV.Text = "SelectCsvFile";
            this.btnCSV.UseSelectable = true;
            this.btnCSV.Click += new System.EventHandler(this.btnCSV_Click);
            // 
            // lblCsv
            // 
            this.lblCsv.Location = new System.Drawing.Point(6, 10);
            this.lblCsv.Name = "lblCsv";
            this.lblCsv.Size = new System.Drawing.Size(335, 22);
            this.lblCsv.TabIndex = 30;
            this.lblCsv.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblCsv.Click += new System.EventHandler(this.lblCsv_Click_1);
            // 
            // btnSendMails
            // 
            this.btnSendMails.BackColor = System.Drawing.Color.White;
            this.btnSendMails.Location = new System.Drawing.Point(13, 13);
            this.btnSendMails.Name = "btnSendMails";
            this.btnSendMails.Size = new System.Drawing.Size(375, 33);
            this.btnSendMails.TabIndex = 0;
            this.btnSendMails.Text = "SendMail";
            this.btnSendMails.UseSelectable = true;
            this.btnSendMails.Click += new System.EventHandler(this.btnSendMails_Click);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(23, 566);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(840, 23);
            this.progressBar.TabIndex = 33;
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(70, 544);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(785, 19);
            this.lblStatus.TabIndex = 34;
            // 
            // lblMailTitle
            // 
            this.lblMailTitle.AutoSize = true;
            this.lblMailTitle.Location = new System.Drawing.Point(9, 15);
            this.lblMailTitle.Name = "lblMailTitle";
            this.lblMailTitle.Size = new System.Drawing.Size(91, 19);
            this.lblMailTitle.TabIndex = 36;
            this.lblMailTitle.Text = "ComposeMail";
            // 
            // lblCopyleft
            // 
            this.lblCopyleft.Location = new System.Drawing.Point(393, 596);
            this.lblCopyleft.Name = "lblCopyleft";
            this.lblCopyleft.Size = new System.Drawing.Size(432, 23);
            this.lblCopyleft.TabIndex = 38;
            this.lblCopyleft.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.imgSelectTrurkey);
            this.groupBox5.Controls.Add(this.imgSelectFrench);
            this.groupBox5.Controls.Add(this.imgSelectNetherlands);
            this.groupBox5.Controls.Add(this.imgSelectSpanish);
            this.groupBox5.Controls.Add(this.imgSelectGerman);
            this.groupBox5.Controls.Add(this.imgSelectEnglish);
            this.groupBox5.Location = new System.Drawing.Point(461, 21);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(189, 32);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            // 
            // imgSelectTrurkey
            // 
            this.imgSelectTrurkey.Image = ((System.Drawing.Image)(resources.GetObject("imgSelectTrurkey.Image")));
            this.imgSelectTrurkey.Location = new System.Drawing.Point(160, 11);
            this.imgSelectTrurkey.Name = "imgSelectTrurkey";
            this.imgSelectTrurkey.Size = new System.Drawing.Size(23, 15);
            this.imgSelectTrurkey.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgSelectTrurkey.TabIndex = 50;
            this.imgSelectTrurkey.TabStop = false;
            this.imgSelectTrurkey.Click += new System.EventHandler(this.imgSelectTrurkey_Click);
            // 
            // imgSelectFrench
            // 
            this.imgSelectFrench.Image = ((System.Drawing.Image)(resources.GetObject("imgSelectFrench.Image")));
            this.imgSelectFrench.Location = new System.Drawing.Point(129, 11);
            this.imgSelectFrench.Name = "imgSelectFrench";
            this.imgSelectFrench.Size = new System.Drawing.Size(25, 15);
            this.imgSelectFrench.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgSelectFrench.TabIndex = 49;
            this.imgSelectFrench.TabStop = false;
            this.imgSelectFrench.Click += new System.EventHandler(this.imgSelectFrench_Click);
            // 
            // imgSelectNetherlands
            // 
            this.imgSelectNetherlands.Image = ((System.Drawing.Image)(resources.GetObject("imgSelectNetherlands.Image")));
            this.imgSelectNetherlands.Location = new System.Drawing.Point(98, 11);
            this.imgSelectNetherlands.Name = "imgSelectNetherlands";
            this.imgSelectNetherlands.Size = new System.Drawing.Size(25, 15);
            this.imgSelectNetherlands.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgSelectNetherlands.TabIndex = 48;
            this.imgSelectNetherlands.TabStop = false;
            this.imgSelectNetherlands.Click += new System.EventHandler(this.imgSelectNetherlands_Click);
            // 
            // imgSelectSpanish
            // 
            this.imgSelectSpanish.Image = ((System.Drawing.Image)(resources.GetObject("imgSelectSpanish.Image")));
            this.imgSelectSpanish.Location = new System.Drawing.Point(67, 11);
            this.imgSelectSpanish.Name = "imgSelectSpanish";
            this.imgSelectSpanish.Size = new System.Drawing.Size(25, 15);
            this.imgSelectSpanish.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgSelectSpanish.TabIndex = 47;
            this.imgSelectSpanish.TabStop = false;
            this.imgSelectSpanish.Click += new System.EventHandler(this.imgSelectSpanish_Click);
            // 
            // imgSelectGerman
            // 
            this.imgSelectGerman.Image = global::SerienbriefMailer.Properties.Resources.germany25x15;
            this.imgSelectGerman.Location = new System.Drawing.Point(6, 11);
            this.imgSelectGerman.Name = "imgSelectGerman";
            this.imgSelectGerman.Size = new System.Drawing.Size(25, 15);
            this.imgSelectGerman.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgSelectGerman.TabIndex = 45;
            this.imgSelectGerman.TabStop = false;
            this.imgSelectGerman.Click += new System.EventHandler(this.imgSelectGerman_Click);
            // 
            // imgSelectEnglish
            // 
            this.imgSelectEnglish.Image = ((System.Drawing.Image)(resources.GetObject("imgSelectEnglish.Image")));
            this.imgSelectEnglish.Location = new System.Drawing.Point(37, 11);
            this.imgSelectEnglish.Name = "imgSelectEnglish";
            this.imgSelectEnglish.Size = new System.Drawing.Size(25, 15);
            this.imgSelectEnglish.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgSelectEnglish.TabIndex = 46;
            this.imgSelectEnglish.TabStop = false;
            this.imgSelectEnglish.Click += new System.EventHandler(this.imgSelectEnglish_Click);
            // 
            // groupBoxPdfMail
            // 
            this.groupBoxPdfMail.Controls.Add(this.imgColumnDecision);
            this.groupBoxPdfMail.Controls.Add(this.imgPdfAttention);
            this.groupBoxPdfMail.Controls.Add(this.groupBox7);
            this.groupBoxPdfMail.Controls.Add(this.lblPdfFile);
            this.groupBoxPdfMail.Location = new System.Drawing.Point(12, 76);
            this.groupBoxPdfMail.Name = "groupBoxPdfMail";
            this.groupBoxPdfMail.Size = new System.Drawing.Size(377, 38);
            this.groupBoxPdfMail.TabIndex = 58;
            this.groupBoxPdfMail.TabStop = false;
            // 
            // imgColumnDecision
            // 
            this.imgColumnDecision.Image = global::SerienbriefMailer.Properties.Resources.warnung;
            this.imgColumnDecision.Location = new System.Drawing.Point(347, 11);
            this.imgColumnDecision.Name = "imgColumnDecision";
            this.imgColumnDecision.Size = new System.Drawing.Size(22, 22);
            this.imgColumnDecision.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgColumnDecision.TabIndex = 31;
            this.imgColumnDecision.TabStop = false;
            this.imgColumnDecision.Visible = false;
            // 
            // imgPdfAttention
            // 
            this.imgPdfAttention.Image = global::SerienbriefMailer.Properties.Resources.warnung;
            this.imgPdfAttention.Location = new System.Drawing.Point(347, 11);
            this.imgPdfAttention.Name = "imgPdfAttention";
            this.imgPdfAttention.Size = new System.Drawing.Size(22, 22);
            this.imgPdfAttention.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgPdfAttention.TabIndex = 31;
            this.imgPdfAttention.TabStop = false;
            this.imgPdfAttention.Visible = false;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.metroLabel1);
            this.groupBox7.Location = new System.Drawing.Point(0, 38);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(382, 38);
            this.groupBox7.TabIndex = 59;
            this.groupBox7.TabStop = false;
            // 
            // metroLabel1
            // 
            this.metroLabel1.Location = new System.Drawing.Point(2, 12);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(363, 23);
            this.metroLabel1.TabIndex = 12;
            this.metroLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxCsv
            // 
            this.groupBoxCsv.Controls.Add(this.lblCsv);
            this.groupBoxCsv.Controls.Add(this.imgCsvOk);
            this.groupBoxCsv.Controls.Add(this.imgCsvAttention);
            this.groupBoxCsv.Location = new System.Drawing.Point(13, 75);
            this.groupBoxCsv.Name = "groupBoxCsv";
            this.groupBoxCsv.Size = new System.Drawing.Size(376, 38);
            this.groupBoxCsv.TabIndex = 61;
            this.groupBoxCsv.TabStop = false;
            // 
            // imgCsvOk
            // 
            this.imgCsvOk.Image = global::SerienbriefMailer.Properties.Resources.ok;
            this.imgCsvOk.Location = new System.Drawing.Point(346, 10);
            this.imgCsvOk.Name = "imgCsvOk";
            this.imgCsvOk.Size = new System.Drawing.Size(22, 22);
            this.imgCsvOk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgCsvOk.TabIndex = 10;
            this.imgCsvOk.TabStop = false;
            // 
            // imgCsvAttention
            // 
            this.imgCsvAttention.Image = global::SerienbriefMailer.Properties.Resources.warnung;
            this.imgCsvAttention.Location = new System.Drawing.Point(346, 10);
            this.imgCsvAttention.Name = "imgCsvAttention";
            this.imgCsvAttention.Size = new System.Drawing.Size(22, 22);
            this.imgCsvAttention.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgCsvAttention.TabIndex = 9;
            this.imgCsvAttention.TabStop = false;
            this.imgCsvAttention.Visible = false;
            // 
            // groupBoxSmtpMain
            // 
            this.groupBoxSmtpMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.groupBoxSmtpMain.Controls.Add(this.metroTextBox1);
            this.groupBoxSmtpMain.Controls.Add(this.checkExchange);
            this.groupBoxSmtpMain.Controls.Add(this.imgSmtpServerSave);
            this.groupBoxSmtpMain.Controls.Add(this.tbxSmtpServer);
            this.groupBoxSmtpMain.Controls.Add(this.groupBox1);
            this.groupBoxSmtpMain.Controls.Add(this.groupBoxSmtpOk);
            this.groupBoxSmtpMain.Controls.Add(this.imgSmtpPasswordSave);
            this.groupBoxSmtpMain.Controls.Add(this.imgSmtpPortSave);
            this.groupBoxSmtpMain.Controls.Add(this.imgSmtpMailaddressSave);
            this.groupBoxSmtpMain.Controls.Add(this.lblSmtpTitle);
            this.groupBoxSmtpMain.Controls.Add(this.tbxSmtpUserMail);
            this.groupBoxSmtpMain.Controls.Add(this.lblSmtpUserMail);
            this.groupBoxSmtpMain.Controls.Add(this.lblSmtpServer);
            this.groupBoxSmtpMain.Controls.Add(this.tbxSmtpPort);
            this.groupBoxSmtpMain.Controls.Add(this.lblSmtpPort);
            this.groupBoxSmtpMain.Controls.Add(this.tbxSmtpPassword);
            this.groupBoxSmtpMain.Controls.Add(this.lblSmtpPassword);
            this.groupBoxSmtpMain.Location = new System.Drawing.Point(461, 257);
            this.groupBoxSmtpMain.Name = "groupBoxSmtpMain";
            this.groupBoxSmtpMain.Size = new System.Drawing.Size(402, 243);
            this.groupBoxSmtpMain.TabIndex = 61;
            this.groupBoxSmtpMain.TabStop = false;
            // 
            // checkExchange
            // 
            this.checkExchange.AutoSize = true;
            this.checkExchange.Location = new System.Drawing.Point(13, 213);
            this.checkExchange.Name = "checkExchange";
            this.checkExchange.Size = new System.Drawing.Size(130, 17);
            this.checkExchange.TabIndex = 63;
            this.checkExchange.Text = "Exchange verwenden";
            this.checkExchange.UseVisualStyleBackColor = true;
            this.checkExchange.CheckedChanged += new System.EventHandler(this.checkExchange_CheckedChanged);
            // 
            // imgSmtpServerSave
            // 
            this.imgSmtpServerSave.Image = global::SerienbriefMailer.Properties.Resources.documentSave;
            this.imgSmtpServerSave.Location = new System.Drawing.Point(369, 78);
            this.imgSmtpServerSave.Name = "imgSmtpServerSave";
            this.imgSmtpServerSave.Size = new System.Drawing.Size(16, 16);
            this.imgSmtpServerSave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgSmtpServerSave.TabIndex = 43;
            this.imgSmtpServerSave.TabStop = false;
            this.imgSmtpServerSave.Visible = false;
            this.imgSmtpServerSave.Click += new System.EventHandler(this.imgSmtpServerSave_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkSsl);
            this.groupBox1.Controls.Add(this.imgSslSave);
            this.groupBox1.Location = new System.Drawing.Point(286, 96);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(103, 39);
            this.groupBox1.TabIndex = 62;
            this.groupBox1.TabStop = false;
            // 
            // chkSsl
            // 
            this.chkSsl.AutoSize = true;
            this.chkSsl.Location = new System.Drawing.Point(12, 15);
            this.chkSsl.Name = "chkSsl";
            this.chkSsl.Size = new System.Drawing.Size(46, 17);
            this.chkSsl.TabIndex = 66;
            this.chkSsl.Text = "SSL";
            this.chkSsl.UseVisualStyleBackColor = true;
            this.chkSsl.CheckedChanged += new System.EventHandler(this.chkSsl_CheckedChanged);
            // 
            // imgSslSave
            // 
            this.imgSslSave.Image = global::SerienbriefMailer.Properties.Resources.documentSave;
            this.imgSslSave.Location = new System.Drawing.Point(83, 15);
            this.imgSslSave.Name = "imgSslSave";
            this.imgSslSave.Size = new System.Drawing.Size(16, 16);
            this.imgSslSave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgSslSave.TabIndex = 65;
            this.imgSslSave.TabStop = false;
            this.imgSslSave.Visible = false;
            this.imgSslSave.Click += new System.EventHandler(this.imgSsl_Click);
            // 
            // groupBoxSmtpOk
            // 
            this.groupBoxSmtpOk.Controls.Add(this.groupBox17);
            this.groupBoxSmtpOk.Controls.Add(this.lblSmtp);
            this.groupBoxSmtpOk.Controls.Add(this.imgSmtpOk);
            this.groupBoxSmtpOk.Controls.Add(this.imgSmtpNotOk);
            this.groupBoxSmtpOk.Location = new System.Drawing.Point(13, 164);
            this.groupBoxSmtpOk.Name = "groupBoxSmtpOk";
            this.groupBoxSmtpOk.Size = new System.Drawing.Size(375, 38);
            this.groupBoxSmtpOk.TabIndex = 62;
            this.groupBoxSmtpOk.TabStop = false;
            this.groupBoxSmtpOk.Visible = false;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.metroLabel4);
            this.groupBox17.Location = new System.Drawing.Point(0, 38);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(382, 38);
            this.groupBox17.TabIndex = 59;
            this.groupBox17.TabStop = false;
            // 
            // metroLabel4
            // 
            this.metroLabel4.Location = new System.Drawing.Point(2, 12);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(363, 23);
            this.metroLabel4.TabIndex = 12;
            this.metroLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // imgSmtpOk
            // 
            this.imgSmtpOk.Image = global::SerienbriefMailer.Properties.Resources.ok;
            this.imgSmtpOk.Location = new System.Drawing.Point(346, 10);
            this.imgSmtpOk.Name = "imgSmtpOk";
            this.imgSmtpOk.Size = new System.Drawing.Size(22, 22);
            this.imgSmtpOk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgSmtpOk.TabIndex = 5;
            this.imgSmtpOk.TabStop = false;
            // 
            // imgSmtpNotOk
            // 
            this.imgSmtpNotOk.Image = global::SerienbriefMailer.Properties.Resources.notOk;
            this.imgSmtpNotOk.Location = new System.Drawing.Point(346, 10);
            this.imgSmtpNotOk.Name = "imgSmtpNotOk";
            this.imgSmtpNotOk.Size = new System.Drawing.Size(22, 22);
            this.imgSmtpNotOk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgSmtpNotOk.TabIndex = 8;
            this.imgSmtpNotOk.TabStop = false;
            this.imgSmtpNotOk.Visible = false;
            // 
            // imgSmtpPasswordSave
            // 
            this.imgSmtpPasswordSave.Image = global::SerienbriefMailer.Properties.Resources.documentSave;
            this.imgSmtpPasswordSave.Location = new System.Drawing.Point(370, 143);
            this.imgSmtpPasswordSave.Name = "imgSmtpPasswordSave";
            this.imgSmtpPasswordSave.Size = new System.Drawing.Size(16, 16);
            this.imgSmtpPasswordSave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgSmtpPasswordSave.TabIndex = 41;
            this.imgSmtpPasswordSave.TabStop = false;
            this.imgSmtpPasswordSave.Visible = false;
            this.imgSmtpPasswordSave.Click += new System.EventHandler(this.imgSmtpPasswordSave_Click);
            // 
            // imgSmtpPortSave
            // 
            this.imgSmtpPortSave.Image = global::SerienbriefMailer.Properties.Resources.documentSave;
            this.imgSmtpPortSave.Location = new System.Drawing.Point(260, 111);
            this.imgSmtpPortSave.Name = "imgSmtpPortSave";
            this.imgSmtpPortSave.Size = new System.Drawing.Size(16, 16);
            this.imgSmtpPortSave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgSmtpPortSave.TabIndex = 42;
            this.imgSmtpPortSave.TabStop = false;
            this.imgSmtpPortSave.Visible = false;
            this.imgSmtpPortSave.Click += new System.EventHandler(this.imgSmtpPortSave_Click);
            // 
            // imgSmtpMailaddressSave
            // 
            this.imgSmtpMailaddressSave.Image = global::SerienbriefMailer.Properties.Resources.documentSave;
            this.imgSmtpMailaddressSave.Location = new System.Drawing.Point(369, 44);
            this.imgSmtpMailaddressSave.Name = "imgSmtpMailaddressSave";
            this.imgSmtpMailaddressSave.Size = new System.Drawing.Size(16, 16);
            this.imgSmtpMailaddressSave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgSmtpMailaddressSave.TabIndex = 44;
            this.imgSmtpMailaddressSave.TabStop = false;
            this.imgSmtpMailaddressSave.Visible = false;
            this.imgSmtpMailaddressSave.Click += new System.EventHandler(this.imgSmtpMailaddressSave_Click);
            // 
            // groupBoxMail
            // 
            this.groupBoxMail.Controls.Add(this.imgDecryptAttachment);
            this.groupBoxMail.Controls.Add(this.imgEncryptAttachment);
            this.groupBoxMail.Controls.Add(this.imgMailBodySave);
            this.groupBoxMail.Controls.Add(this.tbxBody);
            this.groupBoxMail.Controls.Add(this.imgMailNotDeleted);
            this.groupBoxMail.Controls.Add(this.lblEmail);
            this.groupBoxMail.Controls.Add(this.imgNextMail);
            this.groupBoxMail.Controls.Add(this.imgUploadFurtherAttachments);
            this.groupBoxMail.Controls.Add(this.imgBeforeMail);
            this.groupBoxMail.Controls.Add(this.lblMailTitle);
            this.groupBoxMail.Controls.Add(this.imgMailDeleted);
            this.groupBoxMail.Controls.Add(this.imgMailSubjectSave);
            this.groupBoxMail.Controls.Add(this.tbxSubject);
            this.groupBoxMail.Location = new System.Drawing.Point(22, 257);
            this.groupBoxMail.Name = "groupBoxMail";
            this.groupBoxMail.Size = new System.Drawing.Size(402, 289);
            this.groupBoxMail.TabIndex = 62;
            this.groupBoxMail.TabStop = false;
            // 
            // imgDecryptAttachment
            // 
            this.imgDecryptAttachment.Image = global::SerienbriefMailer.Properties.Resources.document_decrypt1;
            this.imgDecryptAttachment.Location = new System.Drawing.Point(103, 172);
            this.imgDecryptAttachment.Name = "imgDecryptAttachment";
            this.imgDecryptAttachment.Size = new System.Drawing.Size(22, 22);
            this.imgDecryptAttachment.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgDecryptAttachment.TabIndex = 71;
            this.imgDecryptAttachment.TabStop = false;
            this.imgDecryptAttachment.Visible = false;
            this.imgDecryptAttachment.Click += new System.EventHandler(this.imgDecryptAttachment_Click);
            // 
            // imgEncryptAttachment
            // 
            this.imgEncryptAttachment.Image = global::SerienbriefMailer.Properties.Resources.document_encrypt1;
            this.imgEncryptAttachment.Location = new System.Drawing.Point(103, 172);
            this.imgEncryptAttachment.Name = "imgEncryptAttachment";
            this.imgEncryptAttachment.Size = new System.Drawing.Size(22, 22);
            this.imgEncryptAttachment.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgEncryptAttachment.TabIndex = 70;
            this.imgEncryptAttachment.TabStop = false;
            this.imgEncryptAttachment.Visible = false;
            this.imgEncryptAttachment.Click += new System.EventHandler(this.imgEncryptAttachment_Click);
            // 
            // imgMailBodySave
            // 
            this.imgMailBodySave.Image = global::SerienbriefMailer.Properties.Resources.documentSave;
            this.imgMailBodySave.Location = new System.Drawing.Point(369, 147);
            this.imgMailBodySave.Name = "imgMailBodySave";
            this.imgMailBodySave.Size = new System.Drawing.Size(16, 16);
            this.imgMailBodySave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgMailBodySave.TabIndex = 35;
            this.imgMailBodySave.TabStop = false;
            this.imgMailBodySave.Click += new System.EventHandler(this.imgMailBodySave_Click);
            // 
            // imgMailNotDeleted
            // 
            this.imgMailNotDeleted.Image = global::SerienbriefMailer.Properties.Resources.mail_mark_unread;
            this.imgMailNotDeleted.Location = new System.Drawing.Point(219, 170);
            this.imgMailNotDeleted.Name = "imgMailNotDeleted";
            this.imgMailNotDeleted.Size = new System.Drawing.Size(22, 22);
            this.imgMailNotDeleted.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgMailNotDeleted.TabIndex = 6;
            this.imgMailNotDeleted.TabStop = false;
            this.imgMailNotDeleted.Visible = false;
            this.imgMailNotDeleted.Click += new System.EventHandler(this.imgMailNotDeleted_Click);
            // 
            // lblEmail
            // 
            this.lblEmail.Location = new System.Drawing.Point(16, 220);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(373, 44);
            this.lblEmail.TabIndex = 25;
            this.lblEmail.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // imgNextMail
            // 
            this.imgNextMail.Image = global::SerienbriefMailer.Properties.Resources.media_skip_forward;
            this.imgNextMail.Location = new System.Drawing.Point(352, 169);
            this.imgNextMail.Name = "imgNextMail";
            this.imgNextMail.Size = new System.Drawing.Size(32, 32);
            this.imgNextMail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgNextMail.TabIndex = 69;
            this.imgNextMail.TabStop = false;
            this.imgNextMail.Click += new System.EventHandler(this.imgNextMail_Click);
            // 
            // imgUploadFurtherAttachments
            // 
            this.imgUploadFurtherAttachments.Image = global::SerienbriefMailer.Properties.Resources.list_add;
            this.imgUploadFurtherAttachments.Location = new System.Drawing.Point(16, 175);
            this.imgUploadFurtherAttachments.Name = "imgUploadFurtherAttachments";
            this.imgUploadFurtherAttachments.Size = new System.Drawing.Size(16, 16);
            this.imgUploadFurtherAttachments.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgUploadFurtherAttachments.TabIndex = 5;
            this.imgUploadFurtherAttachments.TabStop = false;
            this.imgUploadFurtherAttachments.Visible = false;
            this.imgUploadFurtherAttachments.Click += new System.EventHandler(this.UploadFurtherAttachments_Click);
            // 
            // imgBeforeMail
            // 
            this.imgBeforeMail.Image = global::SerienbriefMailer.Properties.Resources.media_skip_backward;
            this.imgBeforeMail.Location = new System.Drawing.Point(317, 169);
            this.imgBeforeMail.Name = "imgBeforeMail";
            this.imgBeforeMail.Size = new System.Drawing.Size(32, 32);
            this.imgBeforeMail.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgBeforeMail.TabIndex = 68;
            this.imgBeforeMail.TabStop = false;
            this.imgBeforeMail.Click += new System.EventHandler(this.imgBeforeMail_Click);
            // 
            // imgMailDeleted
            // 
            this.imgMailDeleted.Image = global::SerienbriefMailer.Properties.Resources.mail_mark_junk;
            this.imgMailDeleted.Location = new System.Drawing.Point(219, 171);
            this.imgMailDeleted.Name = "imgMailDeleted";
            this.imgMailDeleted.Size = new System.Drawing.Size(22, 22);
            this.imgMailDeleted.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgMailDeleted.TabIndex = 4;
            this.imgMailDeleted.TabStop = false;
            this.imgMailDeleted.Visible = false;
            this.imgMailDeleted.Click += new System.EventHandler(this.imgMailDeleted_Click);
            // 
            // imgMailSubjectSave
            // 
            this.imgMailSubjectSave.Image = global::SerienbriefMailer.Properties.Resources.documentSave;
            this.imgMailSubjectSave.Location = new System.Drawing.Point(369, 40);
            this.imgMailSubjectSave.Name = "imgMailSubjectSave";
            this.imgMailSubjectSave.Size = new System.Drawing.Size(16, 16);
            this.imgMailSubjectSave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgMailSubjectSave.TabIndex = 40;
            this.imgMailSubjectSave.TabStop = false;
            this.imgMailSubjectSave.Click += new System.EventHandler(this.imgMailSubjectSave_Click);
            // 
            // groupBoxPdf
            // 
            this.groupBoxPdf.Controls.Add(this.lblPdfTitle);
            this.groupBoxPdf.Controls.Add(this.btnPdfFileSelect);
            this.groupBoxPdf.Controls.Add(this.imgPdfOk);
            this.groupBoxPdf.Controls.Add(this.imgPdfOkNot);
            this.groupBoxPdf.Controls.Add(this.groupBoxPdfMail);
            this.groupBoxPdf.Location = new System.Drawing.Point(23, 80);
            this.groupBoxPdf.Name = "groupBoxPdf";
            this.groupBoxPdf.Size = new System.Drawing.Size(402, 171);
            this.groupBoxPdf.TabIndex = 62;
            this.groupBoxPdf.TabStop = false;
            // 
            // imgPdfOk
            // 
            this.imgPdfOk.Image = global::SerienbriefMailer.Properties.Resources.ok;
            this.imgPdfOk.Location = new System.Drawing.Point(359, 87);
            this.imgPdfOk.Name = "imgPdfOk";
            this.imgPdfOk.Size = new System.Drawing.Size(22, 22);
            this.imgPdfOk.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgPdfOk.TabIndex = 1;
            this.imgPdfOk.TabStop = false;
            this.imgPdfOk.Visible = false;
            // 
            // imgPdfOkNot
            // 
            this.imgPdfOkNot.Image = global::SerienbriefMailer.Properties.Resources.notOk;
            this.imgPdfOkNot.Location = new System.Drawing.Point(359, 87);
            this.imgPdfOkNot.Name = "imgPdfOkNot";
            this.imgPdfOkNot.Size = new System.Drawing.Size(22, 22);
            this.imgPdfOkNot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgPdfOkNot.TabIndex = 3;
            this.imgPdfOkNot.TabStop = false;
            this.imgPdfOkNot.Visible = false;
            // 
            // groupBoxCsvMain
            // 
            this.groupBoxCsvMain.Controls.Add(this.imgCsvSave);
            this.groupBoxCsvMain.Controls.Add(this.lblCsvTitle);
            this.groupBoxCsvMain.Controls.Add(this.btnCSV);
            this.groupBoxCsvMain.Controls.Add(this.groupBoxCsv);
            this.groupBoxCsvMain.Location = new System.Drawing.Point(461, 80);
            this.groupBoxCsvMain.Name = "groupBoxCsvMain";
            this.groupBoxCsvMain.Size = new System.Drawing.Size(402, 122);
            this.groupBoxCsvMain.TabIndex = 63;
            this.groupBoxCsvMain.TabStop = false;
            // 
            // imgCsvSave
            // 
            this.imgCsvSave.Image = global::SerienbriefMailer.Properties.Resources.documentSave;
            this.imgCsvSave.Location = new System.Drawing.Point(370, 49);
            this.imgCsvSave.Name = "imgCsvSave";
            this.imgCsvSave.Size = new System.Drawing.Size(16, 16);
            this.imgCsvSave.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgCsvSave.TabIndex = 63;
            this.imgCsvSave.TabStop = false;
            this.imgCsvSave.Visible = false;
            this.imgCsvSave.Click += new System.EventHandler(this.pbCsvSave_Click);
            // 
            // groupBoxSendMail
            // 
            this.groupBoxSendMail.Controls.Add(this.btnSendMails);
            this.groupBoxSendMail.Location = new System.Drawing.Point(461, 493);
            this.groupBoxSendMail.Name = "groupBoxSendMail";
            this.groupBoxSendMail.Size = new System.Drawing.Size(402, 53);
            this.groupBoxSendMail.TabIndex = 64;
            this.groupBoxSendMail.TabStop = false;
            // 
            // lblStatusPercent
            // 
            this.lblStatusPercent.BackColor = System.Drawing.Color.Blue;
            this.lblStatusPercent.FontWeight = MetroFramework.MetroLabelWeight.Regular;
            this.lblStatusPercent.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.lblStatusPercent.Location = new System.Drawing.Point(25, 544);
            this.lblStatusPercent.Name = "lblStatusPercent";
            this.lblStatusPercent.Size = new System.Drawing.Size(49, 19);
            this.lblStatusPercent.TabIndex = 66;
            this.lblStatusPercent.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblTitle
            // 
            this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.Location = new System.Drawing.Point(61, 27);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(325, 41);
            this.lblTitle.TabIndex = 67;
            this.lblTitle.Text = "label1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::SerienbriefMailer.Properties.Resources.sbm_jpg;
            this.pictureBox1.Location = new System.Drawing.Point(22, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(34, 26);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 73;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.imgInfo_Click);
            // 
            // imgInfo
            // 
            this.imgInfo.Image = global::SerienbriefMailer.Properties.Resources.dialog_information;
            this.imgInfo.Location = new System.Drawing.Point(841, 595);
            this.imgInfo.Name = "imgInfo";
            this.imgInfo.Size = new System.Drawing.Size(22, 22);
            this.imgInfo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.imgInfo.TabIndex = 72;
            this.imgInfo.TabStop = false;
            this.imgInfo.Click += new System.EventHandler(this.imgInfo_Click);
            // 
            // metroTextBox1
            // 
            this.metroTextBox1.Lines = new string[0];
            this.metroTextBox1.Location = new System.Drawing.Point(149, 211);
            this.metroTextBox1.MaxLength = 32767;
            this.metroTextBox1.Name = "metroTextBox1";
            this.metroTextBox1.PasswordChar = '\0';
            this.metroTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox1.SelectedText = "";
            this.metroTextBox1.Size = new System.Drawing.Size(237, 23);
            this.metroTextBox1.TabIndex = 64;
            this.metroTextBox1.UseSelectable = true;
            this.metroTextBox1.Click += new System.EventHandler(this.metroTextBox1_Click);
            // 
            // frm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(886, 624);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.imgInfo);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.groupBoxSendMail);
            this.Controls.Add(this.groupBoxCsvMain);
            this.Controls.Add(this.groupBoxPdf);
            this.Controls.Add(this.groupBoxMail);
            this.Controls.Add(this.groupBoxSmtpMain);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.lblCopyleft);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.lblStatus1);
            this.Controls.Add(this.lblStatusPercent);
            this.Name = "frm";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSelectTrurkey)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSelectFrench)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSelectNetherlands)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSelectSpanish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSelectGerman)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSelectEnglish)).EndInit();
            this.groupBoxPdfMail.ResumeLayout(false);
            this.groupBoxPdfMail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgColumnDecision)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPdfAttention)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBoxCsv.ResumeLayout(false);
            this.groupBoxCsv.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCsvOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCsvAttention)).EndInit();
            this.groupBoxSmtpMain.ResumeLayout(false);
            this.groupBoxSmtpMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSmtpServerSave)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgSslSave)).EndInit();
            this.groupBoxSmtpOk.ResumeLayout(false);
            this.groupBoxSmtpOk.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgSmtpOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSmtpNotOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSmtpPasswordSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSmtpPortSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSmtpMailaddressSave)).EndInit();
            this.groupBoxMail.ResumeLayout(false);
            this.groupBoxMail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgDecryptAttachment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgEncryptAttachment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMailBodySave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMailNotDeleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNextMail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgUploadFurtherAttachments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgBeforeMail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMailDeleted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgMailSubjectSave)).EndInit();
            this.groupBoxPdf.ResumeLayout(false);
            this.groupBoxPdf.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgPdfOk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgPdfOkNot)).EndInit();
            this.groupBoxCsvMain.ResumeLayout(false);
            this.groupBoxCsvMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgCsvSave)).EndInit();
            this.groupBoxSendMail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgInfo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Label lblStatus1;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
        private System.Windows.Forms.PictureBox imgSmtpOk;
        private System.Windows.Forms.PictureBox imgPdfOk;
        private System.Windows.Forms.PictureBox imgCsvOk;
        private System.Windows.Forms.PictureBox imgPdfOkNot;
        private System.Windows.Forms.PictureBox imgSmtpNotOk;
        private System.Windows.Forms.PictureBox imgCsvAttention;
        private MetroFramework.Controls.MetroLabel lblPdfTitle;
        private MetroFramework.Controls.MetroButton btnPdfFileSelect;
        private MetroFramework.Controls.MetroLabel lblPdfFile;
        private MetroFramework.Controls.MetroTextBox tbxSmtpUserMail;
        private MetroFramework.Controls.MetroLabel lblSmtpUserMail;
        private MetroFramework.Controls.MetroLabel lblSmtpServer;
        private MetroFramework.Controls.MetroTextBox tbxSmtpServer;
        private MetroFramework.Controls.MetroLabel lblSmtpPort;
        private MetroFramework.Controls.MetroTextBox tbxSmtpPort;
        private MetroFramework.Controls.MetroLabel lblSmtpPassword;
        private MetroFramework.Controls.MetroTextBox tbxSmtpPassword;
        private MetroFramework.Controls.MetroTextBox tbxSubject;
        private MetroFramework.Controls.MetroTextBox tbxBody;
        private MetroFramework.Controls.MetroLabel lblSmtpTitle;
        private MetroFramework.Controls.MetroLabel lblSmtp;
        private MetroFramework.Controls.MetroLabel lblCsvTitle;
        private MetroFramework.Controls.MetroButton btnCSV;
        private MetroFramework.Controls.MetroLabel lblCsv;
        private MetroFramework.Controls.MetroButton btnSendMails;
        private MetroFramework.Controls.MetroProgressBar progressBar;
        private MetroFramework.Controls.MetroLabel lblStatus;
        private System.Windows.Forms.PictureBox imgMailBodySave;
        private MetroFramework.Controls.MetroLabel lblMailTitle;
        private MetroFramework.Controls.MetroLabel lblCopyleft;
        private System.Windows.Forms.PictureBox imgMailSubjectSave;
        private System.Windows.Forms.PictureBox imgSmtpPasswordSave;
        private System.Windows.Forms.PictureBox imgSmtpPortSave;
        private System.Windows.Forms.PictureBox imgSmtpServerSave;
        private System.Windows.Forms.PictureBox imgSmtpMailaddressSave;
        private System.Windows.Forms.PictureBox imgSelectGerman;
        private System.Windows.Forms.PictureBox imgSelectEnglish;
        private System.Windows.Forms.PictureBox imgMailDeleted;
        private System.Windows.Forms.PictureBox imgUploadFurtherAttachments;
        private System.Windows.Forms.PictureBox imgMailNotDeleted;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBoxPdfMail;
        private System.Windows.Forms.GroupBox groupBox7;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.GroupBox groupBoxCsv;
        private System.Windows.Forms.GroupBox groupBoxSmtpMain;
        private System.Windows.Forms.GroupBox groupBoxMail;
        private System.Windows.Forms.GroupBox groupBoxPdf;
        private System.Windows.Forms.GroupBox groupBoxSmtpOk;
        private System.Windows.Forms.GroupBox groupBox17;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private System.Windows.Forms.GroupBox groupBoxCsvMain;
        private System.Windows.Forms.GroupBox groupBoxSendMail;
        private System.Windows.Forms.PictureBox imgCsvSave;
        private MetroFramework.Controls.MetroLabel lblStatusPercent;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.PictureBox imgBeforeMail;
        private System.Windows.Forms.PictureBox imgNextMail;
        private MetroFramework.Controls.MetroLabel lblEmail;
        private System.Windows.Forms.PictureBox imgPdfAttention;
        private System.Windows.Forms.PictureBox imgColumnDecision;
        private System.Windows.Forms.PictureBox imgSelectSpanish;
        private System.Windows.Forms.PictureBox imgSelectFrench;
        private System.Windows.Forms.PictureBox imgSelectNetherlands;
        private System.Windows.Forms.PictureBox imgSelectTrurkey;
        private System.Windows.Forms.PictureBox imgDecryptAttachment;
        private System.Windows.Forms.PictureBox imgEncryptAttachment;
        private System.Windows.Forms.PictureBox imgInfo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox chkSsl;
        private System.Windows.Forms.PictureBox imgSslSave;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkExchange;
        private MetroFramework.Controls.MetroTextBox metroTextBox1;
    }
}

