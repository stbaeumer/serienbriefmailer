﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace SerienbriefMailer
{
    public class Logs : List<Log>
    {
        public void Export()
        {
            string homeDrive = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

            string filename = homeDrive + "/" + DateTime.Now.ToString("yyyyMMddHHmmss") + "SBM-Protokoll" + ".txt";

            FileStream fileStream = new FileStream(filename, FileMode.Create);

            StreamWriter writer = new StreamWriter(fileStream);

            foreach (var item in this)
            {
                writer.WriteLine(item.Timestamp + " " + item.Record);
            }
            writer.Close();

            Process.Start("notepad.exe", filename);
        }

        internal void AddNewMailsToLogs(Mails mails)
        {
            int mostVerboseRecipient = (mails.Aggregate("", (max, cur) => max.Length > cur.Recipient.Length ? max : cur.Recipient)).Length;
            int mostVerboseSubject = (mails.Aggregate("", (max, cur) => max.Length > cur.Subject.Length ? max : cur.Subject)).Length;
            int mostVerbosePage = 0;

            for (int rowNumber = 0; rowNumber < mails.Count; rowNumber++)
            {
                string pages = "";

                foreach (var mailattachment in mails[rowNumber].mailAttachments)
                {
                    pages = pages + "(F:" + mailattachment.FileNumber + ",P:" + mailattachment.PageNumber + ")";
                    mostVerbosePage = System.Math.Max(pages.ToString().Length, mostVerbosePage);
                }

                this.Add(new Log(rowNumber.ToString("000") + ") " + mails[rowNumber].Recipient.PadRight(mostVerboseRecipient) + "(File:Page) " + pages.PadRight(mostVerbosePage) + " SUBJECT: " + mails[rowNumber].Subject.PadRight(mostVerboseSubject) + " BODY: " + mails[rowNumber].Body));
            }
        }
    }
}
