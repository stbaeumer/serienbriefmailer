﻿namespace SerienbriefMailer
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RecipientsByMail = new System.Windows.Forms.Button();
            this.RecipientsByTag = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // RecipientsByMail
            // 
            this.RecipientsByMail.Location = new System.Drawing.Point(23, 213);
            this.RecipientsByMail.Name = "RecipientsByMail";
            this.RecipientsByMail.Size = new System.Drawing.Size(177, 64);
            this.RecipientsByMail.TabIndex = 0;
            this.RecipientsByMail.Text = "Nur Empfänger auswählen, deren E-Mail unmittelbar ausgelesen werden kann.";
            this.RecipientsByMail.UseVisualStyleBackColor = true;
            this.RecipientsByMail.Click += new System.EventHandler(this.RecipientsByMail_Click);
            // 
            // RecipientsByTag
            // 
            this.RecipientsByTag.Location = new System.Drawing.Point(206, 213);
            this.RecipientsByTag.Name = "RecipientsByTag";
            this.RecipientsByTag.Size = new System.Drawing.Size(192, 64);
            this.RecipientsByTag.TabIndex = 1;
            this.RecipientsByTag.Text = "Nur diejenigen Empfänger auswählen, deren E-Mail-Adresse herleiten lässt.";
            this.RecipientsByTag.UseVisualStyleBackColor = true;
            this.RecipientsByTag.Click += new System.EventHandler(this.RecipientsByTag_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(23, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(375, 110);
            this.label1.TabIndex = 2;
            this.label1.Text = "Die PDF-Dateien enthalten E-Mail-Adressen.\r\nGleichzeitig können auch E-Mail-Adres" +
    "sen aus der CSV-Datei hergeleitet werden.\r\nSie müssen sich entscheiden.";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 349);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RecipientsByTag);
            this.Controls.Add(this.RecipientsByMail);
            this.Name = "Form2";
            this.Text = "Entweder oder ...";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button RecipientsByMail;
        private System.Windows.Forms.Button RecipientsByTag;
        private System.Windows.Forms.Label label1;
    }
}