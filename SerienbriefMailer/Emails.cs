﻿using iTextSharp.text.exceptions;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SerienbriefMailer
{
    public class Mails:BindingList<Mail>
    {
        public int CountRecipientsSelectedByMail { get; set; }
        public int CountRecipientsSeletedByTag { get; set; }


        public int GetIndex(string pRecipient, int pColumnIndex)
        {
            int index = 0;
            
            foreach (var mail in this)
            {
                if (mail.Recipient == pRecipient && mail.Column == pColumnIndex)
                {
                    return index;
                }
                index++;
            }
            return -1;
        }

        protected ArrayList SearchForMailAddressesAndConvertToLower(string pdfText)
        {
            ArrayList recipients = new ArrayList();

            Regex regex = new Regex(@"(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
           + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
             + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
           + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})");

            MatchCollection mailMatches = regex.Matches(pdfText);

            foreach (Match mailMatch in mailMatches)
            {
                recipients.Add(mailMatch.Value.ToString().ToLower());
            }

            return recipients;
        }

        public string SearchAndReplace(string pInput, string pRecipient, Aliases pAliases)
        {
            string output = pInput;

            // If expressions exists in input ...

            Regex aliasRegex = new Regex(@"\[\[(.*?)\]\]", RegexOptions.IgnoreCase);

            MatchCollection aliasMatches = aliasRegex.Matches(pInput);

            foreach (Match aliasMatch in aliasMatches)
            {
                output = output.Replace(aliasMatch.ToString(), pAliases.SearchAndReturnAlias(aliasMatch.ToString(), pRecipient));
            }
            return output;
        }

        internal Mails AddMails(string[] pOpenFileDialog_FileNames, Aliases pAliases)
        {            
            Global.NumberOfPages = 0;

            Mails mails = new Mails();
                        
            // Foreach PDF-File, ...

            int fileNumber = 1;

            foreach (String pdfFile in pOpenFileDialog_FileNames)
            {
                // ... if it is readable for iTextsharp, ...

                PdfReader pdfReader;

                string seletedLanguage = "SerienbriefMailer.Lang.de-DE";
                System.Reflection.Assembly assembly = System.Reflection.Assembly.Load("SerienbriefMailer");

                if (Properties.Settings.Default.SelectedLanguage == "en-UK")
                {
                    seletedLanguage = "SerienbriefMailer.Lang.en-UK";
                }

                System.Resources.ResourceManager resourceManager;
                resourceManager = new System.Resources.ResourceManager(seletedLanguage, assembly);

                try
                {
                    pdfReader = new PdfReader(pdfFile);
                    if (!pdfReader.IsOpenedWithFullPermissions)
                    {
                        throw new Exception(resourceManager.GetString("FileIsProtected") + "\n" + pdfFile.ToString());
                    }
                }
                catch (BadPasswordException)
                {
                    throw new Exception(resourceManager.GetString("FileIsNotOfPdfType") + "\n" + pdfFile.ToString());
                }
                catch (InvalidPdfException)
                {
                    throw new Exception(resourceManager.GetString("FileIsNotOfPdfType") + "\n" + pdfFile.ToString());
                }
                catch (Exception)
                {
                    throw new Exception(resourceManager.GetString("FileIsProtected") + "\n" + pdfFile.ToString());
                }
                     
                // ... the number of pages is being returned.

                Global.NumberOfPages = Global.NumberOfPages + pdfReader.NumberOfPages;

                // Every page of particular file is being searched for addresses.

                for (int pageNumber = 1; pageNumber <= pdfReader.NumberOfPages; pageNumber++)
                {
                    ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();

                    string currentPageText = PdfTextExtractor.GetTextFromPage(pdfReader, pageNumber, strategy);

                    var identifiedMailaddresses = SearchForMailAddressesAndConvertToLower(currentPageText);

                    // For each identified mail-address on particular page... 

                    foreach (var identifiedaddress in identifiedMailaddresses)
                    {
                        int index = mails.GetIndex(identifiedaddress.ToString(), 0);

                        if (index < 0)
                        {
                            // ... if a mail for this recipient is not yet generated it is being created ...

                            string subject = Global.Subject;
                            string body = Global.Body;

                            //if (Global.CsvValid)
                            //{
                            //    subject = SearchAndReplace(subject, identifiedaddress.ToString(), pAliases);
                            //    body = SearchAndReplace(body, identifiedaddress.ToString(), pAliases);
                            //}

                            string password = pAliases.SearchAndReturnPassword(identifiedaddress.ToString());

                            mails.Add(new Mail(identifiedaddress.ToString(), subject, body, new MailAttachment(identifiedaddress.ToString(), fileNumber, pageNumber), password));
                            mails.CountRecipientsSelectedByMail++;
                        }
                        else
                        {
                            // ... otherwise the attachment is added to existing mail.

                            mails[index].mailAttachments.Add(new MailAttachment(identifiedaddress.ToString(), fileNumber, pageNumber));
                        }
                    }
                    
                    // If tag(s) can be found, deduced mails are created.

                    if (Global.CsvValid)
                    {
                        mails = pAliases.AddOrModifyMailsBySearchingForTags(mails, currentPageText, Global.Subject, Global.Body, fileNumber, pageNumber);
                    }
                    else
                    {
                        Properties.Settings.Default.encryptAllAttachments = false;
                        Properties.Settings.Default.Save();
                    }
                }
                   
                fileNumber++;
            }

            // All PLaceholrders are substituded
            if (Global.CsvValid)
            {
                foreach (var item in mails)
                {
                    item.Subject = SearchAndReplace(item.Subject, item.Recipient, pAliases);
                    item.Body = SearchAndReplace(item.Body, item.Recipient, pAliases);
                }    
            }
            
            Global.PdfSuccessfullyReadIn = true;
            return mails;
        }

        internal Mails FilterAllMails(Mails pMails)
        {
            Mails mails = new Mails();

            foreach (var mail in pMails)
            {
                if (mail.Column == 0 && Global.RecipientsByMail)
                {
                    mails.Add(mail);
                }
                if (mail.Column > 0 && !Global.RecipientsByMail)
                {
                    mails.Add(mail);
                }
            }
            return mails;
        }

        internal string TooltipString(Mails mails, int pMaxColumnIndex, Aliases pAliases)
        {
            string toolTipString = "";

            int countByMail = 1;
            int countByTag = 1;

            foreach (Mail mail in mails)
            {
                if (pMaxColumnIndex == 0)
                {
                    if (mail.Column == 0)
                    {
                        toolTipString = toolTipString + "Nr." + countByMail.ToString("000") + " " + mail.Recipient + "\r\n";
                        countByMail++;
                    }    
                }
                else
                {
                    if (mail.Column > 0)
                    {
                        var index = pAliases.FindIndex(i => i.Rows[0] == mail.Recipient);

                        // todo handle emtpty cells

                        if (mail.Column == pMaxColumnIndex)
                        {
                            //pAliases[index].Rows[pMaxColumnIndex].ToString().Replace("\r", "").Length == 0
                            
                            toolTipString = toolTipString + "Nr." + countByTag.ToString("000") + " " + mail.Recipient + " (" + pAliases[0].Rows[pMaxColumnIndex].ToString().Replace("\r", "") + ": " + pAliases[index].Rows[pMaxColumnIndex].ToString().Replace("\r", "") + ")" + "\r\n";
                            countByTag++; 
                        }
                          
                    }
                }
            }

            return toolTipString;
        }
    }
}
