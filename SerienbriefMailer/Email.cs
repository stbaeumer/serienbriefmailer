﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using PgpSharp;

namespace SerienbriefMailer
{
    public class Mail
    {

        public string Recipient { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public string Success { get; set; }

        public bool ReadyForTakeOff { get; set; }


        public bool EncryptAttachment { get; set; }

        // Mails that are not generated directly out of found mail-addresses, 
        // but indirectly out of tags or words that are related to addresses in CSV-file are TagRelated. 
        // For those matches the column index is saved.

        public int Column { get; set; }

        public MailAttachments mailAttachments = new MailAttachments();
        
        public Mail(string pRecipient, string pSubject, string pBody, MailAttachment mailAttachment, string pPassword)
        {
            Recipient = pRecipient;
            Subject = pSubject;
            Body = pBody;
            mailAttachments.Add(mailAttachment);
            ReadyForTakeOff = true;

            Password = pPassword;

            if (Properties.Settings.Default.encryptAllAttachments && pPassword != "")
            {
                EncryptAttachment = true;
            }
            else
            {
                EncryptAttachment = false;
            }
        }
        public Mail(string pRecipient, string pSubject, string pBody, MailAttachment mailAttachment, int pTagRelated, string pPassword)
        {
            Recipient = pRecipient;
            Subject = pSubject;
            Body = pBody;
            mailAttachments.Add(mailAttachment);
            ReadyForTakeOff = true;
            Column = pTagRelated;
            Password = pPassword;

            if (Properties.Settings.Default.encryptAllAttachments && pPassword != "")
            {
                EncryptAttachment = true;
            }
            else
            {
                EncryptAttachment = false;
            }
        }

        public string Password { get; set; }
        public KeyId PublicKey { get; internal set; }

        public static string Encrypt(string pRecipient, string passPhrase)
        {
            return String.Format("{0:X}", (pRecipient+passPhrase).GetHashCode());
        }
    }
}
